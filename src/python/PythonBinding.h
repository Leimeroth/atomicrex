///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/stl_bind.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <arexlib/Atomicrex.h>

/// Helper template function that creates the binding for a getter method returning a matrix.
template <typename ParentClass, typename MatrixClass, const MatrixClass& (ParentClass::*getter_func)() const>
pybind11::cpp_function MatrixGetter()
{
    return pybind11::cpp_function([](pybind11::object& obj) {
        const MatrixClass& tm = (obj.cast<ParentClass&>().*getter_func)();
        pybind11::array_t<typename MatrixClass::element_type> array(
            {tm.row_count(), tm.col_count()},
            {sizeof(typename MatrixClass::element_type), sizeof(typename MatrixClass::column_type)}, tm.elements(), obj);
        // Mark array as read-only.
        reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &=
            ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
        return array;
    });
}

PYBIND11_NAMESPACE_BEGIN(pybind11)
PYBIND11_NAMESPACE_BEGIN(detail)

/// Automatic Python <--> Vector3 conversion
template <typename T>
struct type_caster<atomicrex::Vector_3<T>> {
public:
    bool load(handle src, bool)
    {
        if(!isinstance<sequence>(src)) return false;
        sequence seq = reinterpret_borrow<sequence>(src);
        if(seq.size() != value.size()) throw value_error("Expected sequence of length 3.");
        for(size_t i = 0; i < value.size(); i++) value[i] = seq[i].cast<T>();
        return true;
    }

    static handle cast(const atomicrex::Vector_3<T>& src, return_value_policy /* policy */, handle /* parent */)
    {
        return pybind11::make_tuple(src[0], src[1], src[2]).release();
    }

    PYBIND11_TYPE_CASTER(atomicrex::Vector_3<T>, _("Vector3<") + make_caster<T>::name() + _(">"));
};

/// Automatic Python <--> Point3 conversion
template <typename T>
struct type_caster<atomicrex::Point_3<T>> {
public:
    bool load(handle src, bool)
    {
        if(!isinstance<sequence>(src)) return false;
        sequence seq = reinterpret_borrow<sequence>(src);
        if(seq.size() != value.size()) throw value_error("Expected sequence of length 3.");
        for(size_t i = 0; i < value.size(); i++) value[i] = seq[i].cast<T>();
        return true;
    }

    static handle cast(const atomicrex::Point_3<T>& src, return_value_policy /* policy */, handle /* parent */)
    {
        return pybind11::make_tuple(src[0], src[1], src[2]).release();
    }

    PYBIND11_TYPE_CASTER(atomicrex::Point_3<T>, _("Point3<") + make_caster<T>::name + _(">"));
};

/// Automatic Python <--> Matrix3 conversion
template <typename T>
struct type_caster<atomicrex::Matrix_3<T>> {
public:
    bool load(handle src, bool)
    {
        if(!isinstance<sequence>(src)) return false;
        sequence seq1 = reinterpret_borrow<sequence>(src);
        if(seq1.size() != value.row_count()) throw value_error("Expected sequence of length 3.");
        for(size_t i = 0; i < value.row_count(); i++) {
            if(!isinstance<sequence>(seq1[i])) throw value_error("Expected nested sequence of length 3.");
            sequence seq2 = reinterpret_borrow<sequence>(seq1[i]);
            if(seq2.size() != value.col_count()) throw value_error("Expected nested sequence of length 3.");
            for(size_t j = 0; j < value.col_count(); j++) {
                value(i, j) = seq2[j].cast<T>();
            }
        }
        return true;
    }

    static handle cast(const atomicrex::Matrix_3<T>& src, return_value_policy /* policy */, handle /* parent */)
    {
        return pybind11::array_t<T>({src.row_count(), src.col_count()},
                                    {sizeof(T), sizeof(typename atomicrex::Matrix_3<T>::column_type)}, src.elements())
            .release();
    }

    PYBIND11_TYPE_CASTER(atomicrex::Matrix_3<T>, _("Matrix3<") + make_caster<T>::name + _(">"));
};

PYBIND11_NAMESPACE_END(detail)
PYBIND11_NAMESPACE_END(pybind11)
