"""
This is the Python interface module for atomicrex. It primarily provides
convenient access to the atomicrex job object and the atomic structures
associated with it.
"""

from __future__ import print_function, division
# Make sure native extension module was built against the same  version of the
# Python interpreter that is executing this file. This check serves to detect
# mistakes made by the user, who might be executing this script with a
# different interpreter than the one used to build atomicrex.
#
# Implementation note: The ATOMICREX_PYTHON_VERSION_MAJOR placeholder below
# gets  replaced with the major version number by the CMake script during the
# build process.  Thus, the Python version that was used at build time gets
# hardcoded into this script file and is compared to the runtime version of the
# Python interpreter.
import sys
if sys.version_info[0] != @ATOMICREX_PYTHON_VERSION_MAJOR@:
    sys.stderr.write(
        "----------------------------------------------------------------\n"
        "Warning: You are using a Python {}.x interpreter,\n"
        "but the atomicrex extension module was built against Python "
        "@ATOMICREX_PYTHON_VERSION_MAJOR@.x\n"
        "----------------------------------------------------------------\n"
        "".format(sys.version_info[0]))
from ._atomicrex import set_msglogger_verbosity, logger_verbosity
from .job import Job
from .atomic_structure import AtomicStructure
from .atom_vector_property import AtomVectorProperty
from .scalar_fit_property import ScalarFitProperty
from .potential import Potential
__all__ = ['Job', 'AtomicStructure', 'AtomVectorProperty',
           'ScalarFitProperty', 'Potential']


def __Job_set_verbosity(self, verbosity):
    """Set the verbosity.

    Parameters
    ----------
    verbosity: int
        verbosity level
        (0=none, 1=minimum, 2=medium, 3=maximum, 4=debug)
    """
    if verbosity == 0:
        set_msglogger_verbosity(logger_verbosity.none)
    elif verbosity == 1:
        set_msglogger_verbosity(logger_verbosity.minimum)
    elif verbosity == 2:
        set_msglogger_verbosity(logger_verbosity.medium)
    elif verbosity == 3:
        set_msglogger_verbosity(logger_verbosity.maximum)
    elif verbosity == 4:
        set_msglogger_verbosity(logger_verbosity.debug)
    else:
        print('Invalid verbosity value: {}'.format(verbosity))
        return
Job.set_verbosity = __Job_set_verbosity
