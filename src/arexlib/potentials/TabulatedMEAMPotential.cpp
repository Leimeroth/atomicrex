///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "TabulatedMEAMPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
TabulatedMEAMPotential::TabulatedMEAMPotential(const FPString& id, FitJob* job) : Potential(id, job, "Tabulated-MEAM") {}

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double TabulatedMEAMPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    // Compute charge density and then embedding energy for each atom.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);

        // Skip atoms for which this potential is not enabled.
        if(isAtomTypeEnabled(structure.atomType(i)) == false) continue;

        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        int jnum_half = data.neighborList.numNeighborsHalf(i);
        int nn = 0;
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                nn++;
                double partial_sum = 0;
                neighbor_j->bondData<MEAMBondData>().f = f.eval(rij);
                NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
                for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                    double rik = neighbor_k->r;
                    if(rik < _cutoff) {
                        double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                        partial_sum += neighbor_k->bondData<MEAMBondData>().f * g.eval(cos_theta);
                    }
                }

                rho_value += neighbor_j->bondData<MEAMBondData>().f * partial_sum;
                rho_value += rho.eval(rij);

                // Add pair contribution to total energy.
                totalEnergy += 0.5 * phi.eval(rij);
            }
        }
        totalEnergy += U.eval(rho_value) - zero_atom_energy;
    }

    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double TabulatedMEAMPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    std::vector<Vector3>& forces = structure.atomForces();
    double* Uprime_values = data.perAtomData<double>();

    // Compute charge density and compute embedding energies.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        if(isAtomTypeEnabled(structure.atomType(i)) == false) continue;

        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                double partial_sum = 0;
                neighbor_j->bondData<MEAMBondData>().f = f.eval(rij, neighbor_j->bondData<MEAMBondData>().fprime);
                NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
                for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                    double rik = neighbor_k->r;
                    if(rik < _cutoff) {
                        double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                        partial_sum += neighbor_k->bondData<MEAMBondData>().f * g.eval(cos_theta);
                    }
                }

                rho_value += neighbor_j->bondData<MEAMBondData>().f * partial_sum;
                rho_value += rho.eval(rij);
            }
        }

        double Uprime_i;
        double U_i = U.eval(rho_value, Uprime_i);
        Uprime_values[i] = Uprime_i;

        totalEnergy += U_i - zero_atom_energy;

        Vector3 forces_i = Vector3::Zero();

        // Compute three-body contributions to force.
        neighbor_j = data.neighborList.neighborList(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij >= _cutoff) continue;

            double f_rij = neighbor_j->bondData<MEAMBondData>().f;
            double f_rij_prime = neighbor_j->bondData<MEAMBondData>().fprime;

            Vector3 forces_j = Vector3::Zero();

            NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
            for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                double rik = neighbor_k->r;
                BOOST_ASSERT(rik > 0.0);
                if(rik >= _cutoff) continue;

                double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                double g_prime;
                double g_value = g.eval(cos_theta, g_prime);
                double f_rik = neighbor_k->bondData<MEAMBondData>().f;
                double f_rik_prime = neighbor_k->bondData<MEAMBondData>().fprime;

                double fij = -Uprime_i * g_value * f_rik * f_rij_prime;
                double fik = -Uprime_i * g_value * f_rij * f_rik_prime;

                double prefactor = Uprime_i * f_rij * f_rik * g_prime;
                fij += prefactor / rij * cos_theta;
                fik += prefactor / rik * cos_theta;

                Vector3 fj, fk;
                prefactor /= (rij * rik);

                fij /= rij;
                fj.x() = neighbor_j->delta.x() * fij - neighbor_k->delta.x() * prefactor;
                fj.y() = neighbor_j->delta.y() * fij - neighbor_k->delta.y() * prefactor;
                fj.z() = neighbor_j->delta.z() * fij - neighbor_k->delta.z() * prefactor;

                fik /= rik;
                fk.x() = neighbor_k->delta.x() * fik - neighbor_j->delta.x() * prefactor;
                fk.y() = neighbor_k->delta.y() * fik - neighbor_j->delta.y() * prefactor;
                fk.z() = neighbor_k->delta.z() * fik - neighbor_j->delta.z() * prefactor;

                forces_i -= fj + fk;
                forces_j += fj;
                BOOST_ASSERT(neighbor_k->localIndex < forces.size());
                forces[neighbor_k->localIndex] += fk;
            }

            BOOST_ASSERT(neighbor_j->localIndex < forces.size());
            forces[neighbor_j->localIndex] += forces_j;
        }

        BOOST_ASSERT(i < forces.size());
        forces[i] += forces_i;
    }

    // Compute two-body pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                double rho_prime;
                rho.eval(rij, rho_prime);
                int j = neighbor_j->localIndex;

                double pair_pot_deriv;
                double pair_pot = phi.eval(rij, pair_pot_deriv);

                double fpair = 0;
                if(isAtomTypeEnabled(structure.atomType(i))) {
                    fpair += rho_prime * Uprime_values[i];
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                if(isAtomTypeEnabled(structure.atomType(j))) {
                    fpair += rho_prime * Uprime_values[j];
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                BOOST_ASSERT(i < forces.size() && j < forces.size());
                forces[i] += fvec;
                forces[j] -= fvec;
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void TabulatedMEAMPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // The present implementation of this potential does not support interactions between non-alike atoms.
    for(int i = 0; i < job()->numAtomTypes(); i++)
        for(int j = i + 1; j < job()->numAtomTypes(); j++)
            if(interacting(i, j))
                throw runtime_error(str(
                    format(
                        "Tabulated MEAM potential does not support compound interactions in line %1% of XML file (%2% - %3%).") %
                    potentialElement.lineNumber() % job()->atomTypeName(i) % job()->atomTypeName(j)));

    FPString filename = potentialElement.parsePathParameterElement("param-file");
    parseMEAMFile(filename);
}

/******************************************************************************
* Parses the MEAM spline functionals from the given paremeters file.
******************************************************************************/
void TabulatedMEAMPotential::parseMEAMFile(const FPString& filename)
{
    ifstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open MEAM potential file: %1%") % filename));

    // Skip comment line.
    FPString commentLine;
    getline(stream, commentLine);

    // Parse spline functions.
    parseSplineDefinition(phi, stream);
    parseSplineDefinition(rho, stream);
    parseSplineDefinition(U, stream);
    parseSplineDefinition(f, stream);
    parseSplineDefinition(g, stream);

    // Calculate 'zero-point energy' of single atom in vacuum.
    zero_atom_energy = U.eval(0.0);

    // Determine maximum cutoff radius of all relevant spline functions.
    _cutoff = 0.0;
    if(phi.cutoff() > _cutoff) _cutoff = phi.cutoff();
    if(rho.cutoff() > _cutoff) _cutoff = rho.cutoff();
    if(f.cutoff() > _cutoff) _cutoff = f.cutoff();

    MsgLogger(maximum) << "Maximum cutoff for MEAM potential '" << id() << "' is " << _cutoff << endl;
}

/******************************************************************************
* Parses a single spline definition from the potential file.
******************************************************************************/
void TabulatedMEAMPotential::parseSplineDefinition(CubicSpline& spline, istream& stream)
{
    // Parse number of spline knots.
    int n;
    stream >> n;
    if(n < 2) throw runtime_error("Invalid number of spline knots in MEAM potential file.");

    // Parse first derivatives at beginning and end of spline.
    double d0;
    double dN;
    stream >> d0 >> dN;
    spline.init(n, d0, dN);

    // Skip complete line which consists of four integers.
    // Don't know what the meaning of these numbers is...
    int token;
    stream >> token >> token >> token >> token;

    // Parse knot coordinates.
    for(int i = 0; i < n; i++) {
        double x, y, y2;
        stream >> x >> y >> y2;
        spline.setKnot(i, x, y);
    }
    if(!stream) throw runtime_error("Failed to parse spline definition from MEAM file.");

    spline.prepareSpline();
}

/******************************************************************************
* Writes the potential's parameters to the given output file.
******************************************************************************/
void TabulatedMEAMPotential::writeParamsFile(const FPString& filename) const
{
    // Open output file.
    ofstream stream(filename.c_str());
    if(!stream.is_open())
        throw runtime_error(str(format("Could not open potential parameters file %1% for writing.") % filename));

    // Don't know what the meaning of these numbers is...
    stream << "1 0 0 0 0 1" << endl;

    writeSplineDefinition(phi, stream);
    writeSplineDefinition(rho, stream);
    writeSplineDefinition(U, stream);
    writeSplineDefinition(f, stream);
    writeSplineDefinition(g, stream);
}

/******************************************************************************
* Writes a single spline definition to the output potential file.
******************************************************************************/
void TabulatedMEAMPotential::writeSplineDefinition(const CubicSpline& spline, std::ostream& stream) const
{
    // Write number of spline knots.
    stream << spline.knotCount() << endl;

    // Write first derivatives at beginning and end of spline.
    stream << spline.derivStart() << " " << spline.derivEnd() << endl;

    // Don't know what the meaning of these numbers is...
    stream << "1 0 1 0" << endl;

    // Write coordinates and second derivative of each knot.
    for(int i = 0; i < spline.knotCount(); i++)
        stream << spline.knotX(i) << " " << spline.knotY(i) << " " << spline.knotY2(i) << endl;
}
}
