///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructure.h"
#include "../job/FitJob.h"
#include "../potentials/Potential.h"
#include "../dof/DegreeOfFreedom.h"
#include "../minimizers/Minimizer.h"
#include "../minimizers/LBFGSMinimizer.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Constructor.
 ******************************************************************************/
AtomicStructure::AtomicStructure(const FPString& id, FitJob* job)
    : FitObject(id, job),
      _totalEnergyProperty("total-energy", "eV", job),
      _atomicEnergyProperty("atomic-energy", "eV/atom", job),
      _totalVolumeProperty("total-volume", "A^3", job),
      _atomicVolumeProperty("atomic-volume", "A^3/atom", job),
      _pressureProperty("pressure", "bar", job),
      _bulkModulusProperty("bulk-modulus", "GPa", job),
      _atomForcesProperty("atomic-forces", "eV/A", job)
{
    // Register this structure with the job.
    job->addStructure(this);
    setRelativeWeight(1.0);

    // get copy of the relaxation minimizer from job
    BOOST_ASSERT(job->relaxMinimizer());
    _relaxMinimizer = job->relaxMinimizer()->clone();

    _numAtoms = 0;
    _numLocalAtoms = 0;
    _numGhostAtoms = 0;
    _skinThickness = 0;
    _dirtyFlags = NEIGHBOR_LISTS | ATOM_POSITIONS | STRUCTURE_DOF | POTENTIAL_DATA;
    _virial.fill(0);

    // Initialize and register all properties.
    registerProperty(&_totalEnergyProperty);
    registerProperty(&_atomicEnergyProperty);
    registerProperty(&_totalVolumeProperty);
    registerProperty(&_atomicVolumeProperty);
    registerProperty(&_pressureProperty);
    registerProperty(&_bulkModulusProperty);
    registerProperty(&_atomForcesProperty);

    _pressureTensorProperty[0].initialize("pxx", "bar", job);
    _pressureTensorProperty[1].initialize("pyy", "bar", job);
    _pressureTensorProperty[2].initialize("pzz", "bar", job);
    _pressureTensorProperty[3].initialize("pyz", "bar", job);
    _pressureTensorProperty[4].initialize("pxz", "bar", job);
    _pressureTensorProperty[5].initialize("pxy", "bar", job);
    for(int i = 0; i < 6; i++) registerProperty(&_pressureTensorProperty[i]);

    int cindex = 0;
    for(int i = 1; i <= 6; i++) {
        for(int j = i; j <= 6; j++, cindex++) {
            _elasticConstantProperties[cindex].initialize(str(format("C%1%%2%") % i % j), "GPa", job);
            registerProperty(&_elasticConstantProperties[cindex]);
        }
    }
    BOOST_ASSERT(cindex == sizeof(_elasticConstantProperties) / sizeof(_elasticConstantProperties[0]));

    // These are computed by default.
    _totalEnergyProperty.setOutputEnabled(true);
    _atomicEnergyProperty.setOutputEnabled(true);
    _totalVolumeProperty.setOutputEnabled(true);
    _atomicVolumeProperty.setOutputEnabled(true);

    // Assume periodic boundary conditions by default.
    _pbc.fill(true);

    // Initialize and register all degrees of freedom.
    registerDOF(&_atomCoordinatesDOF);

    // Allocate potential-specific fields.
    _perPotentialData.resize(job->potentials().size());
    for(size_t i = 0; i < job->potentials().size(); i++) {
        _perPotentialData[i].pot = job->potentials()[i];
        _perPotentialData[i].structure = this;
    }
}

/******************************************************************************
 * Setup the cell geometry.
 ******************************************************************************/
void AtomicStructure::setupSimulationCell(const Matrix3& cellVectors, const Point3& cellOrigin, const std::array<bool, 3>& pbc)
{
    if(cellVectors.determinant() == 0.0) throw runtime_error(str(format("Degenerate simulation cell in structure %1%.") % id()));

    _simulationCell = cellVectors;
    _simulationCellOrigin = cellOrigin;
    _reciprocalSimulationCell = cellVectors.inverse();

    if(_saveInitialCell) {
        MsgLogger(debug) << "Saving initial Cell of structure " << id() << std::endl;
        _initialSimulationCell = cellVectors;
        _saveInitialCell = false;
    }

    if(pbc != _pbc) setDirty(NEIGHBOR_LISTS);

    _pbc = pbc;

    // Need to update ghost atoms and possibly neighbor lists.
    setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Change the cell and the atoms.
 ******************************************************************************/
void AtomicStructure::changeSimulationCell(const Matrix3& newCell, bool scaleAtoms)
{
    // Modify cell shape matrix.
    Matrix3 deformation = newCell * reciprocalSimulationCell();
    setupSimulationCell(newCell, simulationCellOrigin(), _pbc);

    // Transform atoms by the given matrix.
    if(scaleAtoms) {
        for(int i = 0; i < numLocalAtoms(); i++)
            atomPositions()[i] = simulationCellOrigin() + deformation * (atomPositions()[i] - simulationCellOrigin());
    }
    // Atomic positions have changed.
    setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Applies an affine transformation to the cell and the atoms.
 ******************************************************************************/
void AtomicStructure::deformSimulationCell(const Matrix3& deformation)
{
    // Modify cell shape matrix.
    setupSimulationCell(deformation * simulationCell(), simulationCellOrigin(), _pbc);

    // Transform atoms by the given matrix.
    for(int i = 0; i < numLocalAtoms(); i++)
        atomPositions()[i] = simulationCellOrigin() + deformation * (atomPositions()[i] - simulationCellOrigin());

    // Atomic positions have changed.
    setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Determines the periodic image of the simulation cell in which the given
 * point is located.
 ******************************************************************************/
Vector3I AtomicStructure::periodicImage(const Point3& p) const
{
    Vector3 rp = reciprocalSimulationCell() * (p - simulationCellOrigin());
    return Vector3I(hasPBC(0) ? (int)floor(rp.x()) : 0, hasPBC(1) ? (int)floor(rp.y()) : 0, hasPBC(2) ? (int)floor(rp.z()) : 0);
}

/******************************************************************************
 * Wraps a point to be inside the simulation cell.
 ******************************************************************************/
Point3 AtomicStructure::wrapPoint(Point3 p) const
{
    // Transform point to reduced cell coordinates.
    Vector3 rp = reciprocalSimulationCell() * (p - simulationCellOrigin());
    for(size_t dim = 0; dim < 3; dim++) {
        if(hasPBC(dim)) {
            if(FloatType s = floor(rp[dim])) p -= s * simulationCell().column(dim);
        }
    }
    return p;
}

/******************************************************************************
 * Wraps a point in reduced coordinates to be inside the simulation cell.
 ******************************************************************************/
Point3 AtomicStructure::wrapReducedPoint(Point3 p) const
{
    for(size_t dim = 0; dim < 3; dim++) {
        if(hasPBC(dim)) {
            p[dim] -= floor(p[dim]);
            BOOST_ASSERT(p[dim] >= 0);
            BOOST_ASSERT(p[dim] < 1);
        }
    }
    return p;
}

/******************************************************************************
 * Deletes all existing atoms and resizes the atoms array.
 ******************************************************************************/
void AtomicStructure::setAtomCount(int numLocalAtoms)
{
    if(numLocalAtoms == _numLocalAtoms) return;

    _numLocalAtoms = numLocalAtoms;
    _numAtoms = numLocalAtoms;
    _numGhostAtoms = 0;
    _atomPositions.resize(_numLocalAtoms, Point3::Origin());
    _atomDisplacements.resize(_numLocalAtoms, Point3::Origin());
    _atomTypes.resize(_numLocalAtoms, 0);
    _atomTags.resize(_numLocalAtoms, 0);
    _atomForcesProperty.setAtomCount(_numLocalAtoms);
    _initialAtomPositions.clear();
    _lastNeighborUpdatePositions.clear();

    setDirty(NEIGHBOR_LISTS);
    setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Updates the structure, creates ghost atoms and builds neighbor lists.
 ******************************************************************************/
void AtomicStructure::updateStructure()
{
    // Determine whether neighbor lists need to be rebuilt.
    bool rebuildNeighborLists = false;
    if(isDirty(NEIGHBOR_LISTS) || _lastNeighborUpdatePositions.size() != numLocalAtoms()) {
        // Need to rebuild every time the number of atoms has changed.
        rebuildNeighborLists = true;
        _lastNeighborUpdatePositions.clear();
        _skinThickness = 0;

        MsgLogger(maximum) << "Neighbor lists of structure '" << id() << "' will be built for the first time." << endl;
    }
    else if(isDirty(ATOM_POSITIONS)) {
        // Check if any atom has moved by a distance larger than half of the skin thickness.
        double threshold = _skinThickness / 2;

        // Reduce threshold by deformation of the simulation cell.
        Matrix3 simCellDelta = _simulationCell - _lastSimulationCell;
        double maxCellDeformation = 0;
        for(int ix = 0; ix <= 1; ix++) {
            for(int iy = 0; iy <= 1; iy++) {
                for(int iz = 0; iz <= 1; iz++) {
                    Vector3 delta = simCellDelta * Vector3(ix * (_ghostAtomImages[0] + 1), iy * (_ghostAtomImages[1] + 1),
                                                           iz * (_ghostAtomImages[1] + 1));
                    maxCellDeformation = std::max(maxCellDeformation, delta.squaredLength());
                }
            }
        }
        maxCellDeformation = sqrt(maxCellDeformation);
        threshold -= 0.5 * maxCellDeformation;

        if(threshold <= 0.0) {
            // Simulation cell has already changed so much, internal motion of atoms doesn't matter.
            rebuildNeighborLists = true;
            MsgLogger(maximum) << "Neighbor lists of structure '" << id()
                               << "' need to be rebuilt because of cell deformation (skin=" << _skinThickness
                               << ", cell_def=" << maxCellDeformation << " threshold=" << threshold << ")." << endl;
        }
        else {
            // Check atomic displacements against threshold.
            double thresholdSquared = threshold * threshold;
            for(size_t i = 0; i < _lastNeighborUpdatePositions.size(); i++) {
                Vector3 delta = _lastNeighborUpdatePositions[i] - _atomPositions[i];
                if(delta.squaredLength() > thresholdSquared) {
                    rebuildNeighborLists = true;
                    MsgLogger(maximum) << "Neighbor lists of structure '" << id()
                                       << "' need to be rebuilt because of atomic displacement (skin=" << _skinThickness
                                       << ", threshold=" << threshold << ")." << endl;
                    break;
                }
            }
        }
    }

    // Assign atom tags if not done yet.
    if(_atomTags.size() < numLocalAtoms()) {
        _atomTags.resize(numLocalAtoms());
        for(int i = 0; i < numLocalAtoms(); i++) {
            _atomTags[i] = i + 1;

            // Check atom type.
            if(atomTypes()[i] < 0 || atomTypes()[i] >= job()->numAtomTypes())
                throw runtime_error(str(format("Atom %1% of structure %2% has an out-of-range atom type: %3%.") % (i + 1) % id() %
                                        atomTypes()[i]));
        }
    }

    // Store a copy of all atomic positions when this function is called for the first time,
    // such that we can revert back to them when necessary.
    if(_initialAtomPositions.empty()) {
        MsgLogger(debug) << "Saving initial atom positions of structure '" << id() << "'." << endl;
        _initialAtomPositions = _atomPositions;
        BOOST_ASSERT(_atomPositions.size() == numLocalAtoms());
    }

    if(rebuildNeighborLists) {
        // Determine a reasonable finite neighbor skin thickness the first time the atoms have moved or the cell has been
        // deformed.
        if(_skinThickness <= 0 && _lastNeighborUpdatePositions.empty() == false) {
            // Skin thickness automatically scales with the average interatomic distance in the structure.
            _skinThickness = 0.8 * pow(std::abs(simulationCell().determinant()) / numLocalAtoms(), 1.0 / 3.0);
        }

        // Make a copy of the current atom positions so that we can detect when the ghost atoms and neighbor lists need
        // to be regenerated again.
        _lastNeighborUpdatePositions.resize(numLocalAtoms());
        std::copy_n(_atomPositions.cbegin(), numLocalAtoms(), _lastNeighborUpdatePositions.begin());
        _lastSimulationCell = _simulationCell;

        // The thickness of of the ghost atom layer is determined by the range of the potential
        // and an extra "skin" thickness, which saves us from rebuilding the ghost atom layer too often.
        double ghostLayerThickness = job()->maximumCutoff() + _skinThickness;

        // Determine how many copies we have to make of the real atoms.
        const int planeVectors[3][2] = {{1, 2}, {2, 0}, {0, 1}};
        FloatType cuts[3][2];
        Vector3 cellNormals[3];
        for(size_t dim = 0; dim < 3; dim++) {
            Vector3 e1 = simulationCell().column(planeVectors[dim][0]);
            Vector3 e2 = simulationCell().column(planeVectors[dim][1]);
            cellNormals[dim] = e1.cross(e2).normalized();
            cuts[dim][0] = cellNormals[dim].dot(reducedToAbsolute(Point3(0, 0, 0)) - Point3::Origin());
            cuts[dim][1] = cellNormals[dim].dot(reducedToAbsolute(Point3(1, 1, 1)) - Point3::Origin());

            if(hasPBC(dim)) {
                _ghostAtomImages[dim] =
                    (int)ceil(ghostLayerThickness / std::abs(simulationCell().column(dim).dot(cellNormals[dim])));
                cuts[dim][0] -= ghostLayerThickness;
                cuts[dim][1] += ghostLayerThickness;
                if(_ghostAtomImages[dim] > 100) {
                    MsgLogger(medium) << "Cell: " << std::endl;
                    MsgLogger(medium) << simulationCell() << std::endl;
                    throw runtime_error(
                        str(format("Supercell of atomic structure '%1%' became too small in spatial direction %2%.") % id() %
                            (dim + 1)));
                }
                if(_ghostAtomImages[dim] <= 0)
                    throw runtime_error(
                        str(format("Supercell dimension of atomic structure '%1%' is invalid in spatial direction %2%.") % id() %
                            (dim + 1)));
            }
            else {
                _ghostAtomImages[dim] = 0;
                cuts[dim][0] -= ghostLayerThickness;
                cuts[dim][1] += ghostLayerThickness;
            }
        }
        _numGhostAtoms = 0;
        _numAtoms = numLocalAtoms();
        _atomPositions.resize(numLocalAtoms());
        _atomDisplacements.resize(numLocalAtoms());
        _atomTypes.resize(numLocalAtoms());
        _atomTags.resize(numLocalAtoms());

        // Create ghost atoms.
        _forwardMapping.clear();
        _reverseMapping.clear();
        _ghostAtomPBCImages.clear();
        int ghostIndex = numLocalAtoms();
        for(int ix = -_ghostAtomImages[0]; ix <= _ghostAtomImages[0]; ix++) {
            for(int iy = -_ghostAtomImages[1]; iy <= _ghostAtomImages[1]; iy++) {
                for(int iz = -_ghostAtomImages[2]; iz <= _ghostAtomImages[2]; iz++) {
                    if(ix == 0 && iy == 0 && iz == 0) continue;
                    Vector3 pbcImage = Vector3(ix, iy, iz);
                    Vector3 shift = simulationCell() * pbcImage;
                    for(int i = 0; i < numLocalAtoms(); i++) {
                        Point3 pimage = atomPositions()[i] + shift;

                        bool isClipped = false;
                        for(size_t dim = 0; dim < 3; dim++) {
                            FloatType d = cellNormals[dim].dot(pimage - Point3::Origin());
                            if(d < cuts[dim][0] || d > cuts[dim][1]) {
                                isClipped = true;
                                break;
                            }
                        }
                        if(!isClipped) {
                            atomPositions().push_back(pimage);
                            atomDisplacements().push_back(atomDisplacements()[i]);
                            atomTypes().push_back(atomTypes()[i]);
                            atomTags().push_back(atomTags()[i]);
                            _reverseMapping.push_back(i);
                            _forwardMapping.push_back(std::make_pair(i, ghostIndex));
                            _ghostAtomPBCImages.push_back(pbcImage);
                            _numGhostAtoms++;
                            _numAtoms++;
                            ghostIndex++;
                        }
                    }
                }
            }
        }
        BOOST_ASSERT(ghostIndex == numAtoms());
        BOOST_ASSERT(_atomPositions.size() == _numAtoms);
        BOOST_ASSERT(_atomDisplacements.size() == _numAtoms);
        BOOST_ASSERT(_atomTypes.size() == _numAtoms);
        BOOST_ASSERT(_atomTags.size() == _numAtoms);

        // Compute potential-specific things like the neighbor lists.
        for(PerPotentialData& perPotential : _perPotentialData) {

            // Allocate per-atom and per-structure data buffers for the potential.
            perPotential.perStructureDataBuffer.resize(perPotential.pot->perStructureDataSize());
            perPotential.perAtomDataBuffer.resize(perPotential.pot->perAtomDataSize() * numAtoms());

            // Build the neighbor list for the potential.
            perPotential.neighborList.build(*this, *perPotential.pot, perPotential.pot->cutoff() + _skinThickness);
        }

        // Reset dirty flags now that we have updated everything.
        clearDirty(ATOM_POSITIONS);
        clearDirty(NEIGHBOR_LISTS);

        // Inform potentials that their stored data for this structure is no longer valid.
        setDirty(POTENTIAL_DATA);
    }
    else if(isDirty(ATOM_POSITIONS)) {
        // Update positions of ghost atoms when local atoms have moved.
        MsgLogger(debug) << "Updating ghost atom positions of structure '" << id() << "'." << endl;
        auto reverseMap = reverseMapping().cbegin();
        auto pbcImage = _ghostAtomPBCImages.cbegin();
        for(auto pos = atomPositions().begin() + numLocalAtoms(); pos != atomPositions().end(); ++pos, ++reverseMap, ++pbcImage) {
            *pos = atomPositions()[*reverseMap] + simulationCell() * (*pbcImage);
        }

        // Update interatomic vectors stored in the neighbor lists.
        for(PerPotentialData& perPotential : _perPotentialData) {
            perPotential.neighborList.update(*this);
        }
        
        // Reset dirty flag now that we have updated everything.
        clearDirty(ATOM_POSITIONS);

        // Inform potentials that their stored data for this structure is no longer valid.
        setDirty(POTENTIAL_DATA);
    }
}

/******************************************************************************
 * Computes all enabled properties of the structures.
 ******************************************************************************/
bool AtomicStructure::computeProperties(bool isFitting)
{
    // Depending on the properties that should be determined,
    // we need to compute the total energy, the forces, or both.
    bool compEnergy = false, compForces = false;
    if(_totalEnergyProperty.shouldCompute(isFitting)) compEnergy = true;
    if(_atomicEnergyProperty.shouldCompute(isFitting)) compEnergy = true;
    if(_pressureProperty.shouldCompute(isFitting)) compForces = true;
    if(_atomForcesProperty.shouldCompute(isFitting)) compForces = true;
    for(int i = 0; i < 6; i++)
        if(_pressureTensorProperty[i].shouldCompute(isFitting)) compForces = true;

    // Do actual energy/force calculation.
    if(compEnergy || compForces) {
        // Relax degrees of freedom first before computing any properties.
        // Abort if relaxation fails.
        if(!relax(isFitting)) {
            return false;
        }

        computeEnergy(compForces, isFitting);

        // Update properties based on calculated value.
        _totalEnergyProperty = _totalEnergy;
        _atomicEnergyProperty = _totalEnergy / numLocalAtoms();
    }

    // Update more properties, which need no computation.
    _totalVolumeProperty = fabs(simulationCell().determinant());
    _atomicVolumeProperty = _totalVolumeProperty / numLocalAtoms();

    // This function calculates the elastic constants of the cell if enabled.
    computeElasticConstants(isFitting);

    return true;
}

/******************************************************************************
 * Relaxes the structure's degrees of freedom such that the total energy is minimized.
 ******************************************************************************/
bool AtomicStructure::relax(bool isFitting)
{
    // Gather degrees of freedom for which relaxation is enabled.
    // Also reset DOFs to initial values if requested.
    vector<DegreeOfFreedom*> completeDOFList;
    listAllDOF(completeDOFList);
    vector<DegreeOfFreedom*> relaxDOFList;
    int numRelaxDOF = 0;
    for(DegreeOfFreedom* dof : completeDOFList) {
        if(dof == &_atomCoordinatesDOF)
            continue;  // Relaxation of atomic degrees of freedom is handled by the computeEnergy() routine.

        if(dof->relax()) {
            relaxDOFList.push_back(dof);
            numRelaxDOF += dof->numScalars();

            if(dof->resetBeforeRelax()) dof->reset();
        }
    }
    MsgLogger(debug) << "Number of DOFs to relax " << numRelaxDOF << endl;
    if(numRelaxDOF == 0) return true;  // Nothing to relax.

    // Determine if we are already starting with an initally energy of nan,
    // if this is the case abort relaxation and fitting step.
    double energy = computeEnergy(false, isFitting);
    if(!std::isfinite(energy)) {
        MsgLogger(medium) << "Relaxing structure " << id() << " aborted. "
                          << "Energy is " << energy << endl;
        return false;
    }

    MsgLogger(maximum) << "Relaxing structure " << id() << " (#dof=" << numRelaxDOF << "):" << endl;

    // Compile list of bound constraints from DOFs.
    vector<Minimizer::BoundConstraints> constraintTypes(numRelaxDOF);
    vector<double> lowerBounds(numRelaxDOF);
    vector<double> upperBounds(numRelaxDOF);
    vector<Minimizer::BoundConstraints>::iterator type_iter = constraintTypes.begin();
    vector<double>::iterator lower_iter = lowerBounds.begin();
    vector<double>::iterator upper_iter = upperBounds.begin();
    for(DegreeOfFreedom* dof : relaxDOFList) dof->getBoundConstraints(type_iter, lower_iter, upper_iter);
    BOOST_ASSERT(type_iter == constraintTypes.end());
    BOOST_ASSERT(lower_iter == lowerBounds.end());
    BOOST_ASSERT(upper_iter == upperBounds.end());

    vector<double> x0(numRelaxDOF);
    double* x0_iter = x0.data();
    for(DegreeOfFreedom* dof : relaxDOFList) dof->exportValue(x0_iter);
    BOOST_ASSERT(x0_iter == x0.data() + x0.size());

    // Define objective function.
    auto function = [this, &relaxDOFList, isFitting](const std::vector<double>& x) -> double {
        const double* src_iter = x.data();
        for(DegreeOfFreedom* dof : relaxDOFList) {
            dof->importValue(src_iter);
        }
        BOOST_ASSERT(src_iter == x.data() + x.size());
        return computeEnergy(false, isFitting);
    };

    // Initialize minimizer.
    LBFGSMinimizer minimizer(job());
    minimizer.prepare(std::move(x0), function);
    minimizer.setConstraints(std::move(constraintTypes), std::move(lowerBounds), std::move(upperBounds));

    Minimizer::MinimizerResult result;
    while((result = minimizer.iterate()) == Minimizer::MINIMIZATION_CONTINUE) {
        MsgLogger(debug) << "     rlx_iter=" << minimizer.itercount() << " E=" << minimizer.value()
                         << " fnorm=" << minimizer.gradientNorm2() << endl;
    }
    if(result == Minimizer::MINIMIZATION_ERROR)
        throw runtime_error(str(format("The minimizer reported an error when relaxing structure '%1%'.") % id()));

    return true;
}

/**
  @brief Computes the total energy and optionally the forces of this structure.
  @details By default, performs relaxation of atomic positions if enabled by the user.
  @param computeForces if True the forces will be evaluated as well
  @param isFitting determines whether this DOF should be relaxed (usually the desired behavior depends on the program phase)
  @param suppressRelaxation if True structural relaxation will be suppressed
 */
double AtomicStructure::computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation)
{
    // Make sure that everything is in place (atoms, neighbor lists etc.).
    updateStructure();

    int numRelaxDOF = _atomCoordinatesDOF.numScalars();

    if(!suppressRelaxation && _atomCoordinatesDOF.relax() && numRelaxDOF > 0) {
        // determine if the atomic positions should be reset
        if(_atomCoordinatesDOF.resetBeforeRelax() && job()->fitMinimizer() && _lastUpdate < job()->fitMinimizer()->itercount()) {
            _atomCoordinatesDOF.reset();
            _lastUpdate = job()->fitMinimizer()->itercount();
            MsgLogger(maximum) << "Resetting atom positions of structure '" << id() << "'." << endl;
        }

        // The internal atomic degrees of freedom must be relaxed first.
        MsgLogger(debug) << "Relaxing atomic d.o.f. of structure " << id() << " (#dof=" << numRelaxDOF << "):" << endl;

        vector<double> x0(numRelaxDOF);
        double* x0_iter = x0.data();
        _atomCoordinatesDOF.exportValue(x0_iter);
        BOOST_ASSERT(x0_iter == x0.data() + x0.size());

        // Define objective function.
        auto function = [this, isFitting](const std::vector<double>& x) -> double {
            const double* src_iter = x.data();
            _atomCoordinatesDOF.importValue(src_iter);
            BOOST_ASSERT(src_iter == x.data() + x.size());
            return computeEnergy(false, isFitting, true);
        };

        // Define gradient function.
        auto gradient = [this, isFitting](const std::vector<double>& x, std::vector<double>& g) -> double {
            // Use analytic gradient (forces) computed by the potential.
            const double* src_iter = x.data();
            _atomCoordinatesDOF.importValue(src_iter);
            BOOST_ASSERT(src_iter == x.data() + x.size());
            double e = computeEnergy(true, isFitting, true);
            auto force_iter = atomForces().begin();
            for(auto g_iter = g.begin(); g_iter != g.end();) {
                *g_iter++ = -force_iter->x();
                *g_iter++ = -force_iter->y();
                *g_iter++ = -force_iter->z();
                ++force_iter;
            }
            ++force_iter;

            // Assertion only valid if we are using a gradient base optimizer
            // BOOST_ASSERT(force_iter == atomForces().end());
            return e;
        };

        // Initialize minimizer.
        // LBFGSMinimizer minimizer(job());

        // Set constraints on the distance every atom can move.
        // vector<Minimizer::BoundConstraints> constraintTypes(numRelaxDOF);
        // vector<double> lowerBounds(numRelaxDOF);
        // vector<double> upperBounds(numRelaxDOF);
        // // maximum move distance is on average atom distance
        // double d = pow(fabs(simulationCell().determinant()) / numLocalAtoms(), 1.0 / 3.0);
        // for(size_t i = 0; i < numRelaxDOF; i++) {
        //     lowerBounds[i] = x0[i] - d;
        //     upperBounds[i] = x0[i] + d;
        //     constraintTypes[i] = Minimizer::BoundConstraints::BOTH_BOUNDS;
        // }
        _relaxMinimizer->prepare(std::move(x0), function, gradient);
        // _relaxMinimizer->setConstraints(std::move(constraintTypes), std::move(lowerBounds), std::move(upperBounds));

        Minimizer::MinimizerResult result;
        while((result = _relaxMinimizer->iterate()) == Minimizer::MINIMIZATION_CONTINUE &&
              _relaxMinimizer->itercount() <= _relaxMinimizer->maximumNumberOfIterations()) {
            MsgLogger(debug) << "        force_rlx_iter=" << _relaxMinimizer->itercount() << " E=" << _relaxMinimizer->value()
                             << " fnorm=" << _relaxMinimizer->gradientNorm2() << endl;
        }

        if(result == Minimizer::MINIMIZATION_ERROR)
            throw runtime_error(str(format("The minimizer reported an error when relaxing structure '%1%'.") % id()));

        // // Test if atom positions are sitting on the bounds after relaxation.
        // // This would be a sign that the allowed move distance is to small.
        // const vector<double>& xf = _relaxMinimizer->bestParameters();
        // const vector<double>& lB = _relaxMinimizer->lowerBounds();
        // const vector<double>& uB = _relaxMinimizer->upperBounds();
        // for(size_t i = 0; i < numRelaxDOF; i++) {
        //     if(xf[i] == lB[i] || xf[i] == uB[i]) {
        //         throw runtime_error(str(format("The minimizer reported an error when relaxing structure '%1%'. An atom came to
        //         "
        //                                        "rest on the boundary of its allowed movement range.") %
        //                                 id()));
        //     }
        // }

        // // Set atom postitions to the optimium value found.
        // const double* src_iter = xf.data();
        // _atomCoordinatesDOF.importValue(src_iter);

        return computeEnergy(computeForces, isFitting, true);
    }
    else {
        _totalEnergy = 0.0;

        BOOST_ASSERT(_perPotentialData.size() == job()->potentials().size());
        if(computeForces) {
            // Reset forces.
            fill(_atomForcesProperty.values(), Vector3::Zero());

            // Reset virial.
            _virial.fill(0);

            // Calculate energy and forces.
            for(PerPotentialData& perPotential : _perPotentialData) {
                _totalEnergy += perPotential.pot->computeEnergyAndForces(*this, perPotential);
            }

#ifdef _DEBUG
            for(int i = 0; i < numLocalAtoms(); i++) {
                BOOST_ASSERT(std::isnan(atomForces()[i].x()) == false);
                BOOST_ASSERT(std::isnan(atomForces()[i].y()) == false);
                BOOST_ASSERT(std::isnan(atomForces()[i].z()) == false);
            }
#endif

#if 0
            // Compute virial.
            vector<Vector3>::const_iterator f = atomForces().begin();
            vector<Point3>::const_iterator p = atomPositions().begin();
            for(int i = 0; i < numLocalAtoms(); i++, ++f, ++p) {
                _virial[0] += f->x() * p->x();
                _virial[1] += f->y() * p->y();
                _virial[2] += f->z() * p->z();
                _virial[3] += f->z() * p->y();
                _virial[4] += f->z() * p->x();
                _virial[5] += f->y() * p->x();
            }
#endif

            // Compute pressure tensor (in bar units).
            double invVolume = 1.0 / fabs(simulationCell().determinant()) * 1.6021765e6;
            for(int i = 0; i < 6; i++) _pressureTensorProperty[i] = _virial[i] * invVolume;
            _pressureProperty = (_virial[0] + _virial[1] + _virial[2]) * invVolume / 3.0;
        }
        else {
            // Calculate energy only.
            for(PerPotentialData& perPotential : _perPotentialData) {
                _totalEnergy += perPotential.pot->computeEnergy(*this, perPotential);
            }
        }

        MsgLogger(debug) << "Computed energy of structure " << id() << "  E=" << _totalEnergy
                         << "  local atoms: " << numLocalAtoms() << "  total atoms: " << numAtoms() << endl;

        // if(!std::isfinite(_totalEnergy))
        // throw runtime_error(str(format("The total energy calculated for structure '%1%' was '%2%'.") % id() %
        // _totalEnergy));

        return _totalEnergy;
    }
}

/******************************************************************************
 * Computes elastic constants of the structure if the corresponding properties
 * are enabled.
 ******************************************************************************/
void AtomicStructure::computeElasticConstants(bool isFitting)
{
    // Calculation of the bulk modulus.
    if(_bulkModulusProperty.shouldCompute(isFitting)) {
        // Save original atom positions and simulation cell shape.
        // We will restore them after the calculation.
        updateStructure();
        vector<Point3> oldAtomPositions(atomPositions().begin(), atomPositions().begin() + numLocalAtoms());
        Matrix3 oldSimulationCell = simulationCell();

        // Expand simulation cell by some epsilon.
        double scale_a = 1.0001;
        Matrix3 expansion(scale_a, 0, 0, 0, scale_a, 0, 0, 0, scale_a);
        deformSimulationCell(expansion);
        // Compute energy at expanded cell volume.
        double e_plus = computeEnergy(false, isFitting);

        // Compress simulation cell by some epsilon.
        Matrix3 compression(1.0 / scale_a / scale_a, 0, 0, 0, 1.0 / scale_a / scale_a, 0, 0, 0, 1.0 / scale_a / scale_a);
        deformSimulationCell(compression);
        // Compute energy at compressed cell volume.
        double e_minus = computeEnergy(false, isFitting);

        // Compute energy at original cell volume.
        copy(oldAtomPositions.begin(), oldAtomPositions.end(), atomPositions().begin());
        setDirty(ATOM_POSITIONS);
        setupSimulationCell(oldSimulationCell, simulationCellOrigin(), pbc());
        double e0 = computeEnergy(false, isFitting);

        // Compute second derivative of E(V) curve from finite differences.
        double V0 = oldSimulationCell.determinant();
        double V_plus = V0 * expansion.determinant();
        double V_minus = V_plus * compression.determinant();
        double Ed2 = 2.0 * (e0 * V_plus + e_minus * V0 + e_plus * V_minus - e0 * V_minus - e_plus * V0 - e_minus * V_plus) /
                     (V0 * V0 * V_plus + V_minus * V_minus * V0 + V_plus * V_plus * V_minus - V0 * V0 * V_minus -
                      V_plus * V_plus * V0 - V_minus * V_minus * V_plus);

        // Compute bulk modulus (V0 * dE^2/dV^2) and convert to GPa units.
        _bulkModulusProperty = V0 * Ed2 * 160.2177;
    }

    // Check if any of the elastic constants C_xy should be computed.
    bool calculateElasticConstants = false;
    for(int i = 0; i < 21; i++) {
        if(_elasticConstantProperties[i].shouldCompute(isFitting)) {
            calculateElasticConstants = true;
            break;
        }
    }

    // Calculation of the elastic constants.
    if(calculateElasticConstants) {
        // Save original atom positions and simulation cell.
        updateStructure();
        vector<Point3> oldAtomPositions(atomPositions().begin(), atomPositions().begin() + numLocalAtoms());
        Matrix3 oldSimulationCell = simulationCell();

        const int voigtMapping[6][2] = {{0, 0}, {1, 1}, {2, 2}, {1, 2}, {0, 2}, {0, 1}};

        double eps = 1e-3;

        int cindex = 0;
        for(int alpha = 0; alpha < 6; alpha++) {
            for(int beta = alpha; beta < 6; beta++, cindex++) {
                if(_elasticConstantProperties[cindex].shouldCompute(isFitting) == false) continue;

                // int i = voigtMapping[alpha][0];
                // int j = voigtMapping[alpha][1];
                int k = voigtMapping[beta][0];
                int l = voigtMapping[beta][1];

                // Compute
                //
                // C_alpha_beta = d(sigma_ij)/d(epsilon_kl)

                // Compute pressure tensor for distorted cell.
                Matrix3 epsilon = Matrix3::Identity();
                epsilon(k, l) += eps;
                deformSimulationCell(epsilon);
                computeEnergy(true, isFitting);
                // Store intermediate value.
                double p1 = pressureTensor(alpha);

                // Restore original cell.
                copy(oldAtomPositions.begin(), oldAtomPositions.end(), atomPositions().begin());
                setDirty(ATOM_POSITIONS);
                setupSimulationCell(oldSimulationCell, simulationCellOrigin(), pbc());

                // Compute pressure tensor for inversely distorted cell.
                epsilon(k, l) -= 2.0 * eps;
                deformSimulationCell(epsilon);
                computeEnergy(true, isFitting);
                // Store intermediate value.
                double p2 = pressureTensor(alpha);

                // Restore original cell.
                copy(oldAtomPositions.begin(), oldAtomPositions.end(), atomPositions().begin());
                setDirty(ATOM_POSITIONS);
                setupSimulationCell(oldSimulationCell, simulationCellOrigin(), pbc());

                // Calculate stress tensor derivatives and convert from bar to GPa units.
                _elasticConstantProperties[cindex] = -(p1 - p2) / (2.0 * eps) * 1e-4;
            }
        }

        // Compute pressure tensor of original cell.
        computeEnergy(true, isFitting);
    }
}

/******************************************************************************
 * Exports the structure to file if filenames have been provided.
 ******************************************************************************/
void AtomicStructure::writeToFile()
{
    if(!_outputFile.empty()) {
        if(_outputFormat == POSCAR) {
            writeToPoscarFile(_outputFile, false);
        }
        else if(_outputFormat == LAMMPS) {
            writeToDumpFile(_outputFile, false);
        }
        else {
            throw runtime_error(str(format("Could not write structure, output format '%1%'' not recognized.") % _outputFormat));
        }
    }
}

/******************************************************************************
 * Exports the structure to a POSCAR file.
 ******************************************************************************/
void AtomicStructure::writeToPoscarFile(const FPString& filename, bool includeGhostAtoms) const
{
    ofstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open poscar file '%1%'' for writing.") % filename));

    stream << "'" << id() << "' written by atomicrex [version " << ATOMICREX_VERSION_STRING << "]" << endl;
    stream << "   1.0" << endl;
    stream << fixed << setprecision(16);

    // Write simulation cell.
    stream << " " << setw(22) << simulationCell()(0, 0) << setw(22) << simulationCell()(1, 0) << setw(22)
           << simulationCell()(2, 0) << endl;
    stream << " " << setw(22) << simulationCell()(0, 1) << setw(22) << simulationCell()(1, 1) << setw(22)
           << simulationCell()(2, 1) << endl;
    stream << " " << setw(22) << simulationCell()(0, 2) << setw(22) << simulationCell()(1, 2) << setw(22)
           << simulationCell()(2, 2) << endl;

    // Count atoms of each type.
    vector<int> typeCount(job()->numAtomTypes(), 0);
    int natoms = includeGhostAtoms ? numAtoms() : numLocalAtoms();
    for(int i = 0; i < natoms; i++) {
        typeCount[atomTypes()[i]]++;
    }

    // Write name line.
    for(int i = 0; i < job()->numAtomTypes(); i++) {
        if(typeCount[i] > 0) stream << job()->atomTypeName(i) << " ";
    }
    stream << endl;

    // Write count line.
    for(int i = 0; i < job()->numAtomTypes(); i++) {
        if(typeCount[i] > 0) stream << typeCount[i] << " ";
    }
    stream << endl;
    stream << "Cartesian" << endl;

    // Write atomic positions.
    for(int type = 0; type < job()->numAtomTypes(); type++) {
        for(int j = 0; j < natoms; j++) {
            if(atomTypes()[j] == type) {
                stream << setw(20) << (atomPositions()[j].x() - simulationCellOrigin().x()) << setw(20)
                       << (atomPositions()[j].y() - simulationCellOrigin().y()) << setw(20)
                       << (atomPositions()[j].z() - simulationCellOrigin().z()) << endl;
            }
        }
    }

    MsgLogger(medium) << "Writing structure '" << id() << "' to " << makePathRelative(filename) << endl;
}

/******************************************************************************
 * Exports the structure to a LAMMPS dump file.
 ******************************************************************************/
void AtomicStructure::writeToDumpFile(const FPString& filename, bool includeGhostAtoms) const
{
    ofstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open dump file '%1%' for writing.") % filename));

    stream << "ITEM: TIMESTEP" << endl;
    stream << "0" << endl;
    if(simulationCell()(0, 1) == 0.0 && simulationCell()(0, 2) == 0.0 && simulationCell()(1, 2) == 0.0) {
        stream << "ITEM: BOX BOUNDS";
        for(int k = 0; k < 3; k++) {
            if(hasPBC(k))
                stream << " pp";
            else
                stream << " ss";
        }
        stream << endl;
        stream << simulationCellOrigin().x() << " " << (simulationCellOrigin().x() + simulationCell()(0, 0)) << endl;
        stream << simulationCellOrigin().y() << " " << (simulationCellOrigin().y() + simulationCell()(1, 1)) << endl;
        stream << simulationCellOrigin().z() << " " << (simulationCellOrigin().z() + simulationCell()(2, 2)) << endl;
    }
    else {
        stream << "ITEM: BOX BOUNDS xy xz yz";
        for(int k = 0; k < 3; k++) {
            if(hasPBC(k))
                stream << " pp";
            else
                stream << " ss";
        }
        stream << endl;
        FloatType xlo = simulationCellOrigin().x();
        FloatType ylo = simulationCellOrigin().y();
        FloatType zlo = simulationCellOrigin().z();
        FloatType xhi = simulationCell().column(0).x() + xlo;
        FloatType yhi = simulationCell().column(1).y() + ylo;
        FloatType zhi = simulationCell().column(2).z() + zlo;
        FloatType xy = simulationCell().column(1).x();
        FloatType xz = simulationCell().column(2).x();
        FloatType yz = simulationCell().column(2).y();
        xlo = min(xlo, xlo + xy);
        xlo = min(xlo, xlo + xz);
        ylo = min(ylo, ylo + yz);
        xhi = max(xhi, xhi + xy);
        xhi = max(xhi, xhi + xz);
        yhi = max(yhi, yhi + yz);
        stream << xlo << " " << xhi << " " << xy << endl;
        stream << ylo << " " << yhi << " " << xz << endl;
        stream << zlo << " " << zhi << " " << yz << endl;
    }

    int natoms = includeGhostAtoms ? numAtoms() : numLocalAtoms();
    stream << "ITEM: NUMBER OF ATOMS" << endl;
    stream << natoms << endl;
    stream << "ITEM: ATOMS id type x y z";
    stream << endl;
    for(int i = 0; i < natoms; i++) {
        stream << (i + 1) << " " << atomTypes()[i] << " ";
        stream << atomPositions()[i].x() << " " << atomPositions()[i].y() << " " << atomPositions()[i].z() << endl;
    }

    MsgLogger(medium) << "Writing structure '" << id() << "' to " << makePathRelative(filename) << endl;
}

/******************************************************************************
 * Parses the structure-specific parameters in the XML element in the job file.
 ******************************************************************************/
void AtomicStructure::parse(XML::Element structureElement)
{
    // Call base class.
    MsgLogger(debug) << "Parse <structure> element (base class FitObject)." << endl;
    FitObject::parse(structureElement);

    // Parse <properties> element.
    MsgLogger(debug) << "Parse <properties> element." << endl;
    XML::Element propertiesElement = structureElement.firstChildElement("properties");
    if(propertiesElement) {
        for(XML::Element propertyElement = propertiesElement.firstChildElement(); propertyElement;
            propertyElement = propertyElement.nextSibling()) {
            // Lookup property.
            FPString propertyId = propertyElement.tag();
            FitProperty* prop = propertyById(propertyId);
            if(prop == nullptr)
                throw runtime_error(str(format("Invalid property element in line %1% of XML file: Unknown property \"%2%\".") %
                                        propertyElement.lineNumber() % propertyId));

            // Let the property object parse the rest.
            prop->parse(propertyElement);
        }
    }

    // Parse <relax-dof> elements.
    MsgLogger(debug) << "Parse <relax-dof> elements." << endl;
    XML::Element relaxDOFElement = structureElement.firstChildElement("relax-dof");
    if(relaxDOFElement) {
        for(XML::Element dofElement = relaxDOFElement.firstChildElement(); dofElement; dofElement = dofElement.nextSibling()) {
            // Lookup DOF.
            FPString dofId = dofElement.tag();
            FPString dofTag = dofElement.parseOptionalStringParameterAttribute("tag");
            DegreeOfFreedom* dof = DOFById(dofId, dofTag);
            if(dof == nullptr)
                throw runtime_error(
                    str(format("Invalid relax element in line %1% of XML file: Unknown degree of freedom \"%2%\" (tag=%3%).") %
                        dofElement.lineNumber() % dofId % (dofTag.empty() ? FPString("<empty>") : dofTag)));

            // Let the DOF parse the rest.
            dof->parseRelax(dofElement);
        }
    }

    // Parse output element
    XML::Element outputElement = structureElement.firstChildElement("output");
    if(outputElement) {
        _outputFile = outputElement.parsePathParameterAttribute("file");
        // Parse format
        FPString formatName = outputElement.parseStringParameterAttribute("format");
        if(formatName == "poscar") {
            _outputFormat = POSCAR;
        }
        else if(formatName == "lammps-dump") {
            _outputFormat = LAMMPS;
        }
        else {
            throw runtime_error(str(format("Invalid output element in line %1% of XML file: Unknown output format \"%2%\". "
                                           "Possible values are \"poscar\" and \"lammps-dump\"") %
                                    outputElement.lineNumber() % formatName));
        }
    }
}
}
