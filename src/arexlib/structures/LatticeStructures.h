///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AtomicStructure.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"

/**
   @brief This file collects the definition of classes that define various simple crystal structures.
 */

namespace atomicrex {

/**
 @brief Base class for cubic lattice structures with one atom type (unary).
 @details This class is used to specify face-centered cubic,
 body-centered cubic, diamond and simple cubic lattices.
*/
class UnaryCubicLatticeStructure : public AtomicStructure
{
protected:

	/// Constructor.
	UnaryCubicLatticeStructure(const FPString& id, FitJob* job);

public:

	/// Parses structure-specific parameters in the XML element in the job file.
	virtual void parse(XML::Element structureElement) override;

	/// Returns the atom type of the lattice.
	int atomType() const { return _atomType; }

	/// Set the atom type of the lattice.
	void setAtomType(double atomType) { _atomType = atomType; }

	/// Returns the lattice parameter of the structure.
	const ScalarDOF& latticeParameter() const { return _latticeParameter; }

	/// Returns the property for computing the relaxed lattice parameter.
	CoupledFitProperty& latticeParameterProperty() { return _latticeParameterProperty; }

private:

	/// The atom type of the lattice.
	int _atomType;

	/// DOF that controls the lattice parameter.
	ScalarDOF _latticeParameter;

	/// This property allows to compute the relaxed lattice parameter.
	CoupledFitProperty _latticeParameterProperty;
};

/**
 @brief Base class for cubic lattice structures with two atom types (binary).
 @details This class is used to specify e.g., rocksalt, zinc blende, and cesium chloride structures.
*/
class BinaryCubicLatticeStructure : public AtomicStructure
{
protected:

	/// Constructor.
	BinaryCubicLatticeStructure(const FPString& id, FitJob* job);

public:

	/// Parses structure-specific parameters in the XML element in the job file.
	virtual void parse(XML::Element structureElement) override;

	/// Returns the first atom type of the lattice.
	int atomTypeA() const { return _atomTypeA; }

	/// Set the first atom type of the lattice.
	void setAtomTypeA(double atomType) { _atomTypeA = atomType; }

	/// Returns the second atom type of the lattice.
	int atomTypeB() const { return _atomTypeB; }

	/// Set the first atom type of the lattice.
	void setAtomTypeB(double atomType) { _atomTypeB = atomType; }

	/// Returns the lattice parameter of the structure.
	const ScalarDOF& latticeParameter() const { return _latticeParameter; }

	/// Returns the property for computing the relaxed lattice parameter.
	CoupledFitProperty& latticeParameterProperty() { return _latticeParameterProperty; }

private:

	/// The atom types of the lattice.
	int _atomTypeA, _atomTypeB;

	/// DOF that controls the lattice parameter.
	ScalarDOF _latticeParameter;

	/// This property allows to compute the relaxed lattice parameter.
	CoupledFitProperty _latticeParameterProperty;

};

/**
 @brief Base class for hexagonal, rhombohedral, and tetragonal lattice structures with one atom type (unary).
 @details This class is used to specify e.g., hexagonal-close packed, omega, and beta-Sn lattices.
*/
class UnaryHexaRhomboTetraLatticeStructure : public AtomicStructure
{
protected:

	/// Constructor.
	UnaryHexaRhomboTetraLatticeStructure(const FPString& id, FitJob* job);

public:

	/// Parses structure-specific parameters in the XML element in the job file.
	virtual void parse(XML::Element structureElement) override;

	/// Returns the atom type of the lattice.
	int atomType() const { return _atomType; }

	/// Set the atom type of the lattice.
	void setAtomType(double atomType) { _atomType = atomType; }

	/// Returns the lattice parameter a of the structure.
	const ScalarDOF& latticeParameter() const { return _latticeParameter; }

	/// Returns the lattice parameter c/a ratio of the structure.
	const ScalarDOF& CtoAratio() const { return _CtoAratio; }

	/// Returns the property for computing the relaxed lattice parameter a.
	CoupledFitProperty& latticeParameterProperty() { return _latticeParameterProperty; }

	/// Returns the property for computing the relaxed lattice parameter c/a.
	CoupledFitProperty& CtoAratioProperty() { return _CtoAratioProperty; }

private:

	/// The atom type of the lattice.
	int _atomType;

	/// DOF that controls the lattice parameter.
	ScalarDOF _latticeParameter;

	/// DOF that controls the lattice parameter c/a ratio.
	ScalarDOF _CtoAratio;

	/// This property allows to compute the relaxed lattice parameter.
	CoupledFitProperty _latticeParameterProperty;

	/// This property allows to compute the relaxed c/a ratio.
	CoupledFitProperty _CtoAratioProperty;
};

/**
 @brief Base class for hexagonal, rhombohedral, and tetragonal lattice structures with two atom types (binary).
 @details This class is used to specify e.g., wurtzite and L10 lattices.
*/
class BinaryHexaRhomboTetraLatticeStructure : public AtomicStructure
{
protected:

	/// Constructor.
	BinaryHexaRhomboTetraLatticeStructure(const FPString& id, FitJob* job);

public:

	/// Parses structure-specific parameters in the XML element in the job file.
	virtual void parse(XML::Element structureElement) override;

	/// Returns the first atom type of the lattice.
	int atomTypeA() const { return _atomTypeA; }

	/// Set the first atom type of the lattice.
	void setAtomTypeA(double atomType) { _atomTypeA = atomType; }

	/// Returns the second atom type of the lattice.
	int atomTypeB() const { return _atomTypeB; }

	/// Set the first atom type of the lattice.
	void setAtomTypeB(double atomType) { _atomTypeB = atomType; }

	/// Returns the lattice parameter a of the structure.
	const ScalarDOF& latticeParameter() const { return _latticeParameter; }

	/// Returns the lattice parameter c/a ratio of the structure.
	const ScalarDOF& CtoAratio() const { return _CtoAratio; }

	/// Returns the property for computing the relaxed lattice parameter a.
	CoupledFitProperty& latticeParameterProperty() { return _latticeParameterProperty; }

	/// Returns the property for computing the relaxed lattice parameter c/a.
	CoupledFitProperty& CtoAratioProperty() { return _CtoAratioProperty; }

private:

	/// The atom types of the lattice.
	int _atomTypeA, _atomTypeB;

	/// DOF that controls the lattice parameter.
	ScalarDOF _latticeParameter;

	/// DOF that controls the lattice parameter c/a ratio.
	ScalarDOF _CtoAratio;

	/// This property allows to compute the relaxed lattice parameter.
	CoupledFitProperty _latticeParameterProperty;

	/// This property allows to compute the relaxed c/a ratio.
	CoupledFitProperty _CtoAratioProperty;
};

/**
   @brief This class defines a face-centered cubic (FCC) lattice.
   @details The lattice structure has only one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <fcc-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </fcc-lattice>
   @endcode
*/
class FCCLatticeStructure : public UnaryCubicLatticeStructure
{
public:

	/// Constructor.
	FCCLatticeStructure(const FPString& id, FitJob* job) : UnaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a body-centered cubic (BCC) lattice.
   @details The lattice structure has only one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <bcc-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </bcc-lattice>
   @endcode
*/
class BCCLatticeStructure : public UnaryCubicLatticeStructure
{
public:

	/// Constructor.
	BCCLatticeStructure(const FPString& id, FitJob* job) : UnaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a hexagonal close-packed lattice (HCP, 2H).
   @details The lattice structure has two degrees of freedom, namely the lattice constants.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <hcp-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> IN_PLANE_LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </hcp-lattice>
   @endcode
*/
class HCPLatticeStructure : public UnaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
	HCPLatticeStructure(const FPString& id, FitJob* job) : UnaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a double hexagonal close-packed lattice (DHCP, 4H).
   @details The lattice structure has two degrees of freedom, namely the lattice constants.
   The DHCP structure is closely related to the HCP structure.
   While HCP corresponds to a stacking sequence AB|AB|..., DHCP is equivalent to a stacking sequence ABAC|ABAC|...
   Hence the name "double" HCP.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code{.xml}
   <dhcp-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> IN_PLANE_LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </dhcp-lattice>
   @endcode
*/
class DHCPLatticeStructure : public UnaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
	DHCPLatticeStructure(const FPString& id, FitJob* job) : UnaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;
};

/**
   @brief This class defines a beta-tin lattice.
   @details The structure has two degrees of freedom, namely the lattice constants (in-plane and out-of-plane).
   The beta-tin structure is mostly relevant for group-IV elements such as Si, Ge, and Sn.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <betaSn-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> IN_PLANE_LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </betaSn-lattice>
   @endcode
*/
class betaSnLatticeStructure : public UnaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
	betaSnLatticeStructure(const FPString& id, FitJob* job) : UnaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a simple cubic lattice.
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <sc-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </sc-lattice>
   @endcode
*/
class SCLatticeStructure : public UnaryCubicLatticeStructure
{
public:

	/// Constructor.
	SCLatticeStructure(const FPString& id, FitJob* job) : UnaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines  diamond lattice.
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <diamond-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </diamond-lattice>
   @endcode
*/
class DIALatticeStructure : public UnaryCubicLatticeStructure
{
public:

	/// Constructor.
	DIALatticeStructure(const FPString& id, FitJob* job) : UnaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a omega-lattice structure.
   @details The structure has two degrees of freedom, namely the lattice constants (in-plane and out-of-plane).
   The omega-structure is primarily of interest in transition metals such as Ti, which features both a low-temperature HCP and a high-temperature BCC phase.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <omega-lattice id="MY_STRUCTURE_NAME">
     <atom-type> ELEMENT_NAME </atom-type>
     <lattice-parameter> IN_PLANE_LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </omega-lattice>
   @endcode
*/
class OMGLatticeStructure : public UnaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
	OMGLatticeStructure(const FPString& id, FitJob* job) : UnaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a sodium chloride lattice (NaCl, B1).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <B1-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </B1-lattice>
   @endcode
*/
class B1LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	B1LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a cesiumchloride lattice (CsCl, B2).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <B2-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </B2-lattice>
   @endcode
*/
class B2LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	B2LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a zincblende lattice (ZnS, B3).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <B3-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </B3-lattice>
   @endcode
*/
class B3LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	B3LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a wurtzite lattice structure (B4).
   @details The lattice structure has three degree of freedom, namely the lattice constants (a,c) and the internal parameter u.
   The internal parameter is 0.375 in the ideal structure.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <B4-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> C_TO_A_RATIO <ca-ratio>
     <u-parameter> U_PARAMETER <u-parameter>
   </B4-lattice>
   @endcode
*/
class B4LatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
	B4LatticeStructure(const FPString& id, FitJob* job);

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

	/// Parses any structure-specific parameters in the XML element in the job file.
	virtual void parse(XML::Element structureElement) override;

	/// Returns the lattice parameter c/a ratio of the structure.
	const ScalarDOF& internalParameterU() const { return _internalParameterU; }

	/// Returns the property for computing the relaxed lattice parameter c/a.
	CoupledFitProperty& internalParameterUProperty() { return _internalParameterUProperty; }

private:

	/// DOF that controls the internal parameter u.
	ScalarDOF _internalParameterU;

	/// This property allows to compute the relaxed internal parameter u.
	CoupledFitProperty _internalParameterUProperty;
};

/**
   @brief This class defines a tungsten carbide (Bh) lattice structure.
   @details The lattice structure has two degrees of freedom, namely the lattice constants (a,c).
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <Bh-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> C_TO_A_RATIO <ca-ratio>
   </Bh-lattice>
   @endcode
   @todo The computation of the elastic constants have not been tested.
*/
class BhLatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
        BhLatticeStructure(const FPString& id, FitJob* job) : BinaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a fluorite lattice (CaF2, C1).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <C1-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </C1-lattice>
   @endcode
*/
class C1LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	C1LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a C15 lattice structure (e.g., Cu2Mg, Fe2Y).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   Resulting structure is A2B.
   @code
   <C15-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </C15-lattice>
   @endcode
*/
class C15LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	C15LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a D2d lattice structure (e.g. CaCu5).
   @details The lattice structure has two degrees of freedom, namely the lattice constants.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   Resulting structure is BA5.
   @code
   <D2d-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </D2d-lattice>
   @endcode
*/
class D2dLatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
        D2dLatticeStructure(const FPString& id, FitJob* job) : BinaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a Ni17Th2 lattice structure (e.g. Ni17Th2, alpha-Fe17Y2).
   @details The lattice structure has two degrees of freedom, namely the lattice constants.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   The resulting structure is A17B2.
   @code
   <Ni17Th2-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </Ni17Th2-lattice>
   @endcode
*/
class Ni17Th2LatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
        Ni17Th2LatticeStructure(const FPString& id, FitJob* job) : BinaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a Th2Zn17 lattice structure (e.g. Th2Zn17, beta-Fe17Y2).
   @details The lattice structure has two degrees of freedom, namely the lattice constants.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   Resulting structure is A17B2.
   @code
   <Th2Zn17-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> CA_RATIO </ca-ratio>
   </Th2Zn17-lattice>
   @endcode
*/
class Th2Zn17LatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
        Th2Zn17LatticeStructure(const FPString& id, FitJob* job) : BinaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a D8a lattice structure (e.g., Mn23Th6, Fe23Y6).
   @details The structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   Resulting structure is A23B6.
   @code
   <D8a-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </D8a-lattice>
   @endcode
*/
class D8aLatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	D8aLatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {};

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a L12 lattice structure (e.g., Ni3Al, Cu3Au).
   @details The lattice structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <L12-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
   </L12-lattice>
   @endcode
*/
class L12LatticeStructure : public BinaryCubicLatticeStructure
{
public:

	/// Constructor.
	L12LatticeStructure(const FPString& id, FitJob* job) : BinaryCubicLatticeStructure(id, job) {};

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

/**
   @brief This class defines a L10 lattice structure (e.g., NiAl, CuAu).
   @details The lattice structure has one degree of freedom, namely the lattice constant.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   @code
   <L10-lattice id="MY_STRUCTURE_NAME">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <ca-ratio> C_TO_A_RATIO <ca-ratio>
   </L10-lattice>
   @endcode
*/
class L10LatticeStructure : public BinaryHexaRhomboTetraLatticeStructure
{
public:

	/// Constructor.
        L10LatticeStructure(const FPString& id, FitJob* job) : BinaryHexaRhomboTetraLatticeStructure(id, job) {}

	/// Updates the structure (atom positions, simulation cell, etc.)
	virtual void updateStructure() override;

};

} // End of namespace
