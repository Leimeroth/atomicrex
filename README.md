# atomicrex — A tool for the construction of interaction models

[**atomicrex**](https://www.atomicrex.org) is a versatile tool for the construction of advanced atomistic models. It is written in C++ and Python.
It was primarily developed to fit interatomic potential models.
Thanks to its flexible generic structure its application range, however, is much larger.
In a general sense, it allows one to develop models that describe a given property as a function of the atomic configuration.
The property in question can be scalar or vectorial in nature, and could represent e.g., total energies and forces.
 
**At the moment, we are no longer actively developing atomicrex.
You can of course still use **atomicrex** as well as modify and extend the source code to fit your needs.**

The source code of **atomicrex** is hosted on [gitlab](https://gitlab.com/atomicrex/atomicrex>).
If you use **atomicrex** in your research please include the following citation in publications or presentations:

* A. Stukowski, E. Fransson, M. Mock, and P. Erhart <br>
  *Atomicrex - a general purpose tool for the construction of atomic interaction models* <br>
  Modelling Simul. Mater. Sci. Eng. **25**, 055003 (2017)<br>
  [doi: 10.1088/1361-651X/aa6ecf](http://dx.doi.org/10.1088/1361-651X/aa6ecf)
