Function and class reference
==============================

.. autosummary::

   atomicrex.Job
   atomicrex.AtomicStructure

.. automodule:: atomicrex
   :members:
   :imported-members:
