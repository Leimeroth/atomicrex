.. highlight:: python
   :linenothreshold: 5

.. _example_python_adding_library_structures:

.. index::
   single: Python interface; example
   single: Examples; Python interface


Example: Adding library structures
==============================================

This example demonstrates how to add library structures via the
:program:`atomicrex` Python interface.

Location
------------------

`examples/python_interface`

Input files
------------------

* `adding_structures_library.py`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_interface/adding_structures_library.py
       :linenos:
       :language: python

* `main.xml`: file with definition of potential

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_interface/main.xml
       :linenos:
       :language: xml

* `potential.xml`: definition of potential form and parameters

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/potential.xml
       :linenos:
       :language: xml


Output (files)
------------------

* The properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_interface/reference_output/log.adding_structures_library
       :linenos:
       :language: text
