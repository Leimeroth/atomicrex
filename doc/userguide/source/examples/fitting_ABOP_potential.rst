.. _example_fitting_ABOP_potential:

.. index::
   single: Analytic bond-order potential (ABOP); example
   single: Examples; Analytic bond-order potential (ABOP)
   single: Examples; Force matching

Fitting an analytic bond-order potential (ABOP)
===============================================================

An :ref:`analytic bond-order potential (ABOP) <abop_potential>` for
cobalt [PetGreWah15]_ is refitted using a molecular dynamics (MD)
snapshot of liquid Co calculated with VASP. This scripts demonstrates
how to use the ABOP potential type and how to perform force matching
[ErcAda94]_.

Location
------------------

`examples/fitting_ABOP_potential`


Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/structures.xml
       :linenos:
       :language: xml

* `POSCAR_0_forces_OUTCAR_108_better`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/POSCAR_0_forces_OUTCAR_108_better
       :linenos:
       :language: text

* `Co.tersoff`: initial parameter set in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/Co.tersoff
       :linenos:
       :language: text


Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_ABOP_potential/reference_output/log
       :linenos:
       :language: text
