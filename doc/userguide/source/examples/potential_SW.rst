.. _example_potential_sw:

.. index::
   single: Stillinger-Weber potential (SW); example
   single: Examples; Stillinger-Weber potential (SW)
   single: Examples; User defined functions
   single: User defined functions; example
   single: Examples; extended markup language (XML) inclusions
   single: extended markup language (XML); Inclusions; example

Stillinger-Weber (SW) potential with user defined functions
=================================================================================================

This example demonstrates the (ab)use of the :ref:`modified embedded
atom method (MEAM) potential <meam_potential>` routine together with
:ref:`user defined functions <user_defined_functions>` to create a
Stillinger-Weber potential. The potential form and parameters have
been taken from [StiWeb84]_.  The example also illustrates the use of
:ref:`XML Inclusions <xml_inclusions>`. Note that the potential is
merely evaluated for a couple of simple lattice structures and none of
the parameters are fitted.

Location
------------------

`examples/potential_SW`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_SW/main.xml
       :linenos:
       :language: xml

* `potential.xml`: potential parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_SW/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input file via
  :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_SW/structures.xml
       :linenos:
       :language: xml


Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/potential_SW/reference_output/log
       :linenos:
       :language: text
