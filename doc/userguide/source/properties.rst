.. _properties:
.. index:: Properties, <properties>

Properties
**********

Structure properties
====================

Given a :ref:`structure <structures>` and an appropriate
:ref:`interaction model <potentials>`, it is possible to calculate
various properties. :program:`atomicrex` provides the functionality
for computing a large number of different properties including
cohesive energy, lattice parameters, elastic properties, or atomic
forces. These properties can be compared to :ref:`target values
<target_value>` and added with an appropriate :ref:`weight
<property_weight>` to the :ref:`objective function
<objective_function>`.

The general format is demonstrated by the following code snippet::

  <structures>
    <fcc-lattice id="FCC">
      <atom-type>Al</atom-type>
      <lattice-parameter>4.0</lattice-parameter>
      <properties>
        <atomic-energy relax='true' fit='true' target='-3.36' relative-weight='100' residual-style='squared'/>
      </properties>
      ...
  </structures>

Here, a :ref:`face-centered cubic (FCC) <fcc>` lattice is defined for
aluminum with an initial lattice parameter of 4.0. The cohesive energy
per atom (``<atomic-energy>``) is specified as a property.

:program:`atomicrex` distinguishes scalar properties and vector
properties. The majority of properties listed :ref:`below
<property_list>` is scalar in nature. The most common vector property
are the atomic forces. The behavior of a property can be customized
using the attributes of the ``<property>`` element:

.. _residual_style:
.. _target_value:
.. _property_weight:
.. _property_bounds:

* ``fit``: If this attribute is set to ``True`` the property is
  included during fitting and thus added to the :ref:`objective
  function <objective_function>`.
  
* ``target`` (scalar properties only): This attribute sets the target
  value for the property.

* ``min``, ``max`` (scalar properties only): These attributes enable
  one to constrain the structural relaxation, which is halted if the
  bounds defined here are exceedeed. This can be useful to prevent
  optimization algorithms (in particular global optimizers) from
  crashing.
  
* ``relative-weight``: This attribute sets the weight by which the
  property contributes to the :ref:`objective function
  <objective_function>`; it corresponds to the parameter :math:`w_P`
  in :ref:`the definition of the objective function
  <objective_function>`.
  
* ``residual-style``: This attribute determines how the residual
  associated with this property is calculated, which enters in the
  summation of the :ref:`objective function
  <objective_function>`. Possible values are

  * ``squared``

    .. math::
       r = \left( \Delta A / \delta \right)^2
    
  * ``squared-relative``

    .. math::
       r = \left( \Delta A / A^{\text{target}} \right)^2

  * ``absolute-diff``

    .. math::
       r = \left| \Delta A / \delta \right|

  Here, :math:`\Delta A = A^{\text{predicted}} - A^{\text{target}}`,
  where :math:`A^{\text{predicted}}` and :math:`A^{\text{target}}` are
  the predicted and target values of the property. The parameter
  :math:`\delta` is the ``tolerance`` value specified for this
  property.
  
* ``tolerance``: The tolerance parameter :math:`\delta` used in the
  calculation of the residual (compare ``residual-style``). The
  default value is 1.0. The usage of this parameter is discussed
  :ref:`here <objective_function>`.

* ``relax``: If this attribute is set to ``True`` the structure is
  *relaxed before* the property is evaluated.
  
* ``output``: If this attribute is set to ``True`` the property is
  written to standard output during the :ref:`output phase
  <output_phase>` and also during the :ref:`fitting phase
  <training_phase>` if ``fit="True"``.
  
* ``output-all`` (vector properties only): If this attribute is set to
  ``True`` the all components of the vector will be printed rather
  than just the residual of the entire vector.

.. index:: <atomic-energy>, <lattice-parameter>, <ca-ratio>,
           <atomic-distance>, <atom-forces>, <bulk-modulus>,
           <pressure>, <pxx>, <pyy>, <pzz>, <pyz>, <pxz>, <pxy>, <Cij>
.. index::
   single: Properties; atomic energy
   single: Properties; energy per atom
   single: Properties; atomic forces
   single: Properties; bulk modulus
   single: Properties; pressure
   single: Properties; stress tensor
   single: Properties; stiffness tensor
   single: Properties; elastic constants
   single: Properties; lattice constants
   single: Properties; internal structural parameters
.. _cohesive_energy:
.. _atomic_forces:
.. _lattice_parameters:
.. _elastic_constants:
.. _property_list:

The following table provides an overview of the various properties
that can be computed (and fitted). In addition to the properties
listed also the various parameters associated with the
:ref:`pre-defined crystal structures <predefined_structures>` can be
fitted such as ``<atom-distance>`` (:ref:`dimer structure
<dimer_structure>`), ``<lattice-parameter>``, ``<ca-ratio>``,
``<u-parameter>`` (:ref:`wurtzite structure <wurtzite_structure>`)
etc.

======================== ============ ================================================================================================
Element                  Type         Description
======================== ============ ================================================================================================
``<atomic-energy>``        Scalar       Energy per atom (in eV/atom)
``<atomic-forces>``        Vector       Atomic forces (in eV/A)
``<bulk-modulus>``         Scalar       Bulk modulus (in GPa)
``<pressure>``             Scalar       Hydrostatic pressure (in Bar)
``<pxx>``                  Scalar       :math:`xx` component of stress tensor (1st component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<pyy>``                  Scalar       :math:`yy` component of stress tensor (2nd component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<pzz>``                  Scalar       :math:`zz` component of stress tensor (3rd component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<pyz>``                  Scalar       :math:`yz` component of stress tensor (4th component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<pxz>``                  Scalar       :math:`xz` component of stress tensor (5th component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<pxy>``                  Scalar       :math:`xy` component of stress tensor (6th component in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_, in Bar)
``<Cij>``                  Scalar       Stiffness tensor in `Voigt notation <https://en.wikipedia.org/wiki/Voigt_notation>`_;
                                        ``i`` and ``j`` are integers between 1 and 6; e.g. ``<C11>``, ``<C12>`` etc. (in GPa)
======================== ============ ================================================================================================

The calculation of elastic properties, in particular of both "clamped
ion" and "relaxed ion" elastic constants for a hexagonal lattice
structure, is demonstrated in examples :ref:`[1]
<example_elastic_constants_Cu>` and :ref:`[2]
<example_elastic_constants_GaN>`.

Derived properties
====================

.. _derived_property:
.. index::
   single: Properties; derived property
   single: <derived-properties>
   single: <derived-property>

Derived properties are not directly linked to a structure, but may be
calculated from the properties of one or multiple structures. This
allows one to fit to properties such as defect formation energies,
surface energies and energy differences between different crystal
structures. Even complicated properties like dislocation cores and
phonon frequencies could be in principle represented.

The following code snippet demonstrates the general format::

  <derived-properties>
    <derived-property id="bccFe-Ev" unit="eV" fit="true" target="2.1" relative-weight="1"
                   equation="[Vac.total-energy] - 127*[bcc-Fe.atomic-energy] "/>
  </derived-properties>

Here, the vacancy formation energy in BCC Fe is calculated by
substracting the atomic energy of 127 Fe atoms from the total energy
of a supercell containing a vacancy.  Derived properties are defined
in a separate xml block below the structure definitions. They can be
grouped in the same way as structures. Usage of derived properties is
demonstrated in this :ref:`example
<example_derived_properties>`.

Compared to "normal" properties, a derived property features two
additional attributes:

* ``id``: The name of the derived property.
* ``equation``: The equation is parsed by the math parser and used to
  calculate the value of the derived property. Structure properties
  are referred to using the format ``[structure.property]``.
