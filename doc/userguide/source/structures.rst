.. index:: Structures, <structures>

.. _structures:

Structures
***********************

Structures are one of two principal entities in :program:`atomicrex`,
the other one being :ref:`potentials <potentials>`. They are inserted
in the ``<structures>`` block of the input file.

A rather large database of pre-defined structures is available; in
addition, arbitrary structures can be included either using the
``<user-structure>`` construct or via external structure input
files. Structures are associated with a set of :ref:`properties
<properties>` that are computed either during the :ref:`fitting
<training_phase>` or :ref:`output phase <output_phase>` depending on
the settings in the :ref:`job file <jobfile>`. The ``<group>`` element
allows one to :ref:`create groups of structures
<grouping_structures>`, which is very convenient when handling a large
number of input structures.

.. _structure_weight:

Each structure can be :ref:`assigned an individual weight
<objective_function>`, which can be set via the ``relative-weight``
attribute of the structure element. The default weight is one.

It is possible to export the final structure after the fitting process to a data
file. The following code snippet exemplifies the command for this functionality
in the input file::

  <output file="structure.dump" format="lammps-dump"/>

This creates a lammps dump file named "structure.dump". Possible file formats
are "lammps-dump" and "poscar". This functionality is also used in this
:ref:`example <example_derived_properties>` to visualize the vacancy created
there.

.. _predefined_structures:

.. index:: Pre-defined structures

Pre-defined structures
====================================================

For convenience and computational efficiency many common crystal
structures have been implemented directly into
:program:`atomicrex`. These structures are compiled in the list that
follows below. Generally, structures can be identified using either
their conventional abbreviation (e.g., FCC for face-centered cubic) or
their `Strukturbericht
<https://commons.wikimedia.org/wiki/Strukturbericht>`_ symbol (e.g.,
A1 for FCC).

.. _dimer_structure:
.. index::
   single: Dimer
   single: Structures; dimer

Dimer
--------------

* ``<dimer>``: This sets up a dimer. The dimer has one degree of
  freedom, namely the interatomic distance. The following code snippet
  exemplifies the definition of the dimer structure in the input
  file::

   <dimer-structure id="SiC_dimer">
     <atom-type-A> Si </atom-type-A>
     <atom-type-B> C </atom-type-B>
     <atom-distance> 1.75 </atom_distance>
   </dimer-structure>



Unary crystal structures
-----------------------------------------------

.. _fcc:
.. index::
   single: Structures; face-centered cubic (FCC), A1
   single: <fcc-lattice>
   single: <A1-lattice>

* ``<fcc-lattice>``, ``<A1-lattice>``: This sets up a `face-centered cubic
  (FCC) lattice
  <http://www.chemtube3d.com/solidstate/_ccp%28final%29.htm>`_. The
  lattice structure has only one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

     <fcc-lattice id="Cu_fcc">
       <atom-type> Cu </atom-type>
       <lattice-parameter> 3.615 </lattice-parameter>
     </fcc-lattice>

.. _bcc:
.. index::
   single: Structures; body-centered cubic (BCC), A2
   single: <bcc-lattice>
   single: <A2-lattice>

* ``<bcc-lattice>``, ``<A2-lattice>``: This sets up a `body-centered cubic
  (BCC) lattice
  <http://www.chemtube3d.com/solidstate/_bcc%28final%29.htm>`_. The
  lattice structure has only one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

     <bcc-lattice id="Mo_bcc">
       <atom-type> Mo </atom-type>
       <lattice-parameter> 3.147 </lattice-parameter>
     </bcc-lattice>

.. _hcp:
.. index::
   single: Structures; hexagonal close packed (HCP), A3
   single: <hcp-lattice>
   single: <A3-lattice>

* ``<hcp-lattice>``, ``<A3-lattice>``: This sets up a `hexagonal
  close-packed lattice (HCP, 2H)
  <http://www.chemtube3d.com/solidstate/_hcp%28final%29.htm>`_. The
  lattice structure has two degrees of freedom, namely the lattice
  constants. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

      <hcp-lattice id="Ti_hcp">
        <atom-type> Ti </atom-type>
	<lattice-parameter> 2.95 </lattice-parameter>
	<ca-ratio> 1.588 </ca-ratio>
      </hcp-lattice>

.. _dhcp:
.. index::
   single: Structures; double hexagonal close packed (DHCP)
   single: <dhcp-lattice>

* ``<dhcp-lattice>``: This sets up a double hexagonal close-packed
  lattice (DHCP, 4H). The lattice structure has two degrees of
  freedom, namely the lattice constants.  The DHCP structure is
  closely related to the HCP structure. While HCP corresponds to a
  stacking sequence AB|AB|..., DHCP is equivalent to a stacking
  sequence ABAC|ABAC|... Hence the name "double" HCP. The following
  code snippet exemplifies the definition of the lattice structure in
  the input file::

     <dhcp-lattice id="Ti_hcp">
       <atom-type> Ti </atom-type>
       <lattice-parameter> 2.95 </lattice-parameter>
       <ca-ratio> 3.176 </ca-ratio>
     </dhcp-lattice>

.. index::
   single: Structures; beta-Sn, A5
   single: <betaSn-lattice>
   single: <A5-lattice>

* ``<betaSn-lattice>``, ``<A5-lattice>``: This sets up a beta-tin
  lattice. The structure has two degrees of freedom, namely the
  lattice constants (in-plane and out-of-plane). The beta-tin
  structure is mostly relevant for group-IV elements such as Si, Ge,
  and Sn. The following code snippet exemplifies the definition of the
  lattice structure in the input file::

     <betaSn-lattice id="Si_betatin">
       <atom-type> Si </atom-type>
       <lattice-parameter> 3.1 </lattice-parameter>
       <ca-ratio> 0.9 </ca-ratio>
     </betaSn-lattice>

.. index::
   single: Structures; simple cubic (SC), Ah
   single: <sc-lattice>

* ``<sc-lattice>``, ``<A5-lattice>``: This sets up a `simple cubic lattice
  <http://www.chemtube3d.com/solidstate/_simplecubic%28final%29.htm>`_. The
  structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

     <sc-lattice id="Si_sc">
       <atom-type> Si </atom-type>
       <lattice-parameter> 2.6 </lattice-parameter>
     </sc-lattice>

.. index::
   single: Structures; diamond, A4
   single: <diamond-lattice>
   single: <A4-lattice>

* ``<diamond-lattice>``, ``<A4-lattice>``: This sets up a diamond
  lattice. The structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

    <diamond-lattice id="Si_dia">
      <atom-type> Si </atom-type>
      <lattice-parameter> 5.1 </lattice-parameter>
    </diamond-lattice>

.. index::
   single: Structures; omega-phase
   single: <Ah-lattice>
   single: <omega-lattice>

* ``<omega-lattice>``: This sets up an omega-lattice structure. The
  structure has two degrees of freedom, namely the lattice constants
  (in-plane and out-of-plane). The omega-structure is primarily of
  interest in transition metals such as Ti, Zr, or Hf, which feature
  both a low-temperature :ref:`hexagonal close packed (HCP) <hcp>` and
  a high-temperature :ref:`body-centered cubic (BCC) phase <bcc>`. The
  following code snippet exemplifies the definition of the lattice
  structure in the input file::

     <omega-lattice id="Ti_omega">
       <atom-type> Ti </atom-type>
       <lattice-parameter> 4.57 </lattice-parameter>
       <ca-ratio> 0.618 </ca-ratio>
     </omega-lattice>


Binary and ternary crystal structures
-------------------------------------------

.. index::
   single: Structures; rocksalt (NaCl), B1
   single: <B1-lattice>
   single: <NaCl-lattice>

* ``<B1-lattice>``, ``<NaCl-lattice>``: This sets up a `sodium chloride
  lattice (NaCl, B1)
  <http://www.chemtube3d.com/solidstate/_rocksalt%28final%29.htm>`_. The
  structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

   <B1-lattice id="ZnO_rocksalt">
     <atom-type-A> Zn </atom-type-A>
     <atom-type-B> O </atom-type-B>
     <lattice-parameter> 2.75 </lattice-parameter>
   </B1-lattice>

.. index::
   single: Structures; caesium chloride (CsCl), B2
   single: <B2-lattice>
   single: <CsCl-lattice>

* ``<B2-lattice>``, ``<CsCl-lattice>``: This sets up a `caesium chloride
  lattice (CsCl, B2)
  <http://www.chemtube3d.com/solidstate/_CsCl%28final%29.htm>`_. The
  structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

   <B2-lattice id="ZnO_caesiumchloride">
     <atom-type-A> Zn </atom-type-A>
     <atom-type-B> O </atom-type-B>
     <lattice-parameter> 2.64 </lattice-parameter>
   </B2-lattice>

.. index::
   single: Structures; zincblende, B3
   single: <B3-lattice>
   single: <zincblende-lattice>

* ``<B3-lattice>``, ``<zincblende-lattice>``: This sets up a `zinc blende
  lattice (ZnS, B3)
  <http://www.chemtube3d.com/solidstate/_blende%28final%29.htm>`_. The
  structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file::

   <B3-lattice id="ZnO_zincblende">
     <atom-type-A> Zn </atom-type-A>
     <atom-type-B> O </atom-type-B>
     <lattice-parameter> 4.55 </lattice-parameter>
   </B3-lattice>

.. _wurtzite_structure:
.. index::
   single: Structures; wurtzite, B4
   single: <B4-lattice>
   single: <wurtzite-lattice>

* ``<B4-lattice>``, ``<wurtzite-lattice>``: This sets up a `wurtzite
  lattice structure (B4)
  <http://www.chemtube3d.com/solidstate/_wurtzite%28final%29.htm>`_. The
  lattice structure has three degree of freedom, namely the lattice
  constants (:math:`a`, :math:`c`) and the internal parameter
  :math:`u`, which equals 0.375 in the ideal structure. The following
  code snippet exemplifies the definition of the lattice structure in
  the input file::

   <B4-lattice id="ZnO_wurtzite">
     <atom-type-A> Zn </atom-type-A>
     <atom-type-B> O </atom-type-B>
     <lattice-parameter> 3.242 </lattice-parameter>
     <ca-ratio> 1.6 <ca-ratio>
     <u-parameter> 0.38 <u-parameter>
   </B4-lattice>

.. index::
   single: Structures; tungsten carbide (WC), Bh
   single: <Bh-lattice>
   single: <WC-lattice>

* ``<Bh-lattice>``, ``<WC-lattice>``: This sets up a tungsten carbide (Bh)
  lattice structure. The lattice structure has two degrees of freedom,
  namely the lattice constants (:math:`a`, :math:`c`). The following
  code snippet exemplifies the definition of the lattice structure in
  the input file::

   <Bh-lattice id="WC">
     <atom-type-A> W </atom-type-A>
     <atom-type-B> C </atom-type-B>
     <lattice-parameter> 2.907 </lattice-parameter>
     <ca-ratio> 0.97 <ca-ratio>
   </Bh-lattice>

.. index::
   single: Structures; fluorite, C1
   single: <C1-lattice>
   single: <fluorite-lattice>

* ``<C1-lattice>``, ``<fluorite-lattice>``: This sets up a `fluorite
  lattice
  <http://www.chemtube3d.com/solidstate/_fluorite%28final%29.htm>`_
  (CaF\ :sub:`2`\ , C1). The structure has one degree of freedom,
  namely the lattice constant. The following code snippet exemplifies
  the definition of the lattice structure in the input file::

   <C1-lattice id="CaF2">
     <atom-type-A> Ca </atom-type-A>
     <atom-type-B> F </atom-type-B>
     <lattice-parameter> 5.462 </lattice-parameter>
   </C1-lattice>

.. index::
   single: Structures; C15 lattice
   single: <C15-lattice>

* ``<C15-lattice>``: This sets up a C15 lattice structure (e.g., Cu\
  :sub:`2`\ Mg, Fe\ :sub:`2`\ Y). The structure has one degree of
  freedom, namely the lattice constant. The following code snippet
  exemplifies the definition of the lattice structure in the input
  file, which creates a structure with the chemical sum equation A\
  :sub:`2`\ B::

   <C15-lattice id="Fe2Y">
     <atom-type-A> Fe </atom-type-A>
     <atom-type-B> Y </atom-type-B>
     <lattice-parameter> 3.0 </lattice-parameter>
   </C15-lattice>

.. index::
   single: Structures; D8a lattice
   single: <D8a-lattice>

* ``<D8a-lattice>``: This sets up a D8a lattice structure (e.g., Mn\
  :sub:`23`\ Th\ :sub:`6`\ , Fe\ :sub:`23`\ Y\ :sub:`6`\ ). The
  structure has one degree of freedom, namely the lattice
  constant. The following code snippet exemplifies the definition of
  the lattice structure in the input file, which creates a structure
  with the chemical sum equation A\ :sub:`23`\ B\ :sub:`6`\ ::

   <D8a-lattice id="Fe23Y6">
     <atom-type-A> Fe </atom-type-A>
     <atom-type-B> Y </atom-type-B>
     <lattice-parameter> 4.5</lattice-parameter>
   </D8a-lattice>

.. index::
   single: Structures; L12
   single: <L12-lattice>

* ``<L12-lattice>``: This sets up a L1\ :sub:`2` `lattice structure
  <http://www.chemtube3d.com/solidstate/SSCu3Au.htm>`_ (e.g., Ni\
  :sub:`3`\ Al, Cu\ :sub:`3`\ Au). The lattice structure has one
  degree of freedom, namely the lattice constant. The following code
  snippet exemplifies the definition of the lattice structure in the
  input file::

   <L12-lattice id="Fe3Pt">
     <atom-type-A> Fe </atom-type-A>
     <atom-type-B> Pt </atom-type-B>
     <lattice-parameter> 3.74 </lattice-parameter>
   </L12-lattice>

.. index::
   single: Structures; L10
   single: <L10-lattice>

* ``<L10-lattice>``: This sets up a L1\ :sub:`0` lattice structure
  (e.g., NiAl, CuAu).  The lattice structure has one degree of
  freedom, namely the lattice constant. The following code snippet
  exemplifies the definition of the lattice structure in the input
  file::

   <L10-lattice id="FePt">
     <atom-type-A> Fe </atom-type-A>
     <atom-type-B> Pt </atom-type-B>
     <lattice-parameter> 3.872 </lattice-parameter>
     <ca-ratio> 0.973 <ca-ratio>
   </L10-lattice>




.. _user_defined_structures:
.. index::
   single: User defined structures
   single: Structures; user defined
   single: <user-structure>

User defined structures
====================================================

In addition to the pre-defined structures described above,
:program:`atomicrex` allows one to include user defined
structures. The latter can be provided either directly in the input
file or via an external file.

.. _structure_in_input_file:
.. index::
   single: User defined structures; via job file

Specifying a user defined structure in the input file
-----------------------------------------------------------------

Structures can be specified directly in the input file using the
``<user-structure>`` element, which is customized using several
additional (optional) elements. This is illustrated in the following block, which
defines a conventional unitcell of the rocksalt structure for GaN, allows atomic relaxation and sets target values of atomic forces property to 0. It
is taken from one of the :ref:`examples
<example_relaxing_user_structure>`::

  <user-structure id="B1">
    <pbc x="true" y="true" z="true" />
    <cell>
      <a1 x="4.22" y="0" z="0" />
      <a2 x="0" y="4.22" z="0" />
      <a3 x="0" y="0" z="4.22" />
      <atoms>
        <atom type="Ga" x="0.1" y="0.0" z="0.0" reduced="true" />
        <atom type="Ga" x="0.56" y="0.5" z="0.2" reduced="true" />
        <atom type="Ga" x="0.5" y="0.0" z="0.5" reduced="true" />
        <atom type="Ga" x="0.0" y="0.5" z="0.5" reduced="true" />

        <atom type="N" x="0.5" y="0.2" z="0.0" reduced="true" />
        <atom type="N" x="0.0" y="0.5" z="0.0" reduced="true" />
        <atom type="N" x="0.0" y="0.0" z="0.5" reduced="true" />
        <atom type="N" x="0.5" y="0.6" z="0.8" reduced="true" />
      </atoms>
    </cell>

    <relax-dof>
      <atom-coordinates/>
    </relax-dof>

    <atomic-forces>
      <force i=0 x=0.0 y=0.0 z=0.0/>
      <force i=1 x=0.0 y=0.0 z=0.0/>
      <force i=2 x=0.0 y=0.0 z=0.0/>
      <force i=3 x=0.0 y=0.0 z=0.0/>
      <force i=4 x=0.0 y=0.0 z=0.0/>
      <force i=5 x=0.0 y=0.0 z=0.0/>
      <force i=6 x=0.0 y=0.0 z=0.0/>
      <force i=7 x=0.0 y=0.0 z=0.0/>
    </atomic-forces>
  </user-structure>

.. index:: <pbc>, <cell>, <atoms>, <atom>

The various elements have the following meaning:

* ``<pbc>``: Set the periodic boundary along the three Cartesian
  directions :math:`x`, :math:`y`, and :math:`z`.
* ``<cell>``: Set the cell metric defined by the three vectors ``<a1>``,
  ``<a2>``, and ``<a3>``.
* ``<atoms>``: Container for a list of atoms that are each specified by
  a set of ``<atom>`` elements.
* ``<atom>``: Define an atom where the type is set via the ``type``
  attribute and the positions are set using the ``x``, ``y``, and ``z``
  attributes. The `reduced` attribute determines whether the values
  provided via ``x``, ``y``, and ``z`` are given in absolute units
  (``reduced="False"``) or direct/reduced units, i.e., in units of the
  cell vectors (``reduced="True"``).

.. index:: <relax-dof>, <atom-coordinates>

The ``<relax-dof>`` block generally allows one which degrees of freedom
ought to be relaxed. In the case of a structure defined via the
``<user-structure>`` element only the atomic coordinates can be relaxed
(but not the cell metric), which is accomplished by inserting the
``<atom-coordinates>`` element.

In the present example, the lattice constant is directly incorporated
in the ``<cell>`` block. It is also possible to set the lattice constant
by applying a :ref:`deformation <deformations>`, in which case the
lattice constant enters as a scaling factor. This is demonstrated in
:ref:`this example <example_relaxing_user_structure>`.

.. _structure_from_file:
.. index::
   single: User defined structures; from external file

User defined structures from external files
-----------------------------------------------------------------

It is also possible to read structures from external files. This is
particularly useful when dealing with a large number of configurations
e.g., for force matching. At present :program:`atomicrex` supports
input files in `Lammps dump format
<http://lammps.sandia.gov/doc/dump.html>`_ and a modified `POSCAR
format <http://cms.mpi.univie.ac.at/vasp/vasp/POSCAR_file.html>`_ that
also contains the atomic forces. Some :ref:`utility scripts
<utility_scripts>` are provided that can handle these file formats.

The inclusion of external structure files in the input file is
straightforward and merely requires the insertion of a format specific
element. To include a structure in Lammps dump format one uses the
``<lammps-dump>`` element::

  <user-structure id="my_structure_from_lammps">
    <lammps-dump>my_lammps_file.dump</lammps-dump>
  </user-structure>

Similarly, for POSCAR style files one employs the ``<poscar-file>``
element::

  <user-structure id="my_structure_from_somewhere_else">
    <poscar-file>my_POSCAR_file</poscar-file>
  </user-structure>


Lammps dump format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The `Lammps dump format <http://lammps.sandia.gov/doc/dump.html>`_
allows one to provide a number of atom specific
fields. :program:`atomicrex` parses the standard fields related to
position (``x/y/z``, ``xu/yu/yz``, and ``xs/ys/zs``), the force
(``fx/fy/fz``), the type (``type``) and the identifier (``id``). In
addition, it attempts to read atomic displacement data (``dx/dy/dz``),
which is used for fitting certain force constant models. The first
lines of a typical Lammps dump file suitable for force matching with
:program:`atomicrex` are shown below::

  ITEM: TIMESTEP
  0
  ITEM: NUMBER OF ATOMS
  108
  ITEM: BOX BOUNDS pp pp pp
  0         8.7915
  0        15.2273
  0.160821 14.1956
  ITEM: ATOMS id xu yu zu fx fy fz
  1       0.44          0               0.160821222         -2.309895         -0.016848        -0.000000
  2       1.46524925    2.53788615      0.160821222          0.101288          0.468496        -0.000000
  ...

Modified POSCAR format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:program:`atomicrex` can also read structures provided in the `POSCAR
format <http://cms.mpi.univie.ac.at/vasp/vasp/POSCAR_file.html>`_. The
original version of this format does, however, not support the
inclusion of force data. :program:`atomicrex` therefore employs a
modified format, in which the components of the atomic force vector
are provided in columns 4 to 6 of the lines that provide the atomic
coordinates. The first lines of a typical POSCAR style input file are
shown below. This file was taken from :ref:`this example
<example_fitting_eam_potential>`::

  Al
   1.0
      10.935    0.000    0.000
       0.000   10.935    0.000
       0.000    0.000   10.935
   108
  Cartesian
    0.51167  10.49959  10.69792   -1.068787  0.248625 -0.496509
    0.45777   1.43782   1.71744    0.871483  0.863685 -1.458461
    2.27776  10.75694   1.66683   -0.220888 -1.198273 -0.600977
    ...

.. _point_defect_structures:
.. index:: Point defects, <point-defect>

Point defect structures
========================

For convenience and computational efficiency point defect structures have been
implemented directly into :program:`atomicrex`. The following code snippet
exemplifies the definition of a point defect structure in the input file::

      <point-defect id="Vac">
        <atom-type-A>Fe</atom-type-A>
        <lattice-type>bcc</lattice-type>
        <cell-size>4</cell-size>
        <lattice-parameter> 2.86 </lattice-parameter>
        <defects>
           <vacancy index="1" />
           <substitutional index="1" atom-type="Y" />
           <interstitial x="0.5625" y="0.5625" z="0.5625" atom-type="Y" />
        </defects>
        <properties>
           <lattice-parameter equalto="bcc-Fe.lattice-parameter"/>
        </properties>
      </point-defect>

The upper part defines the super cell which contains the point defect.

* `atom-type-A`: The atom type of the super cell lattice.
* `lattice-type`: The crystal lattice of the super cell. Available are `bcc`, `fcc` and `hcp`.
  Point defect structures are based on the cubic cell definitions.
* `cell-size`: The super cell size, 4 corresponds to a 4x4x4 grid of unit cells.
* `lattice-parameter`: The lattice parameter of the unit cell.
* `ca-ration`: The ca-ratio of the unit cell (only lattice type `hcp`).

The `<defects>` block adds point defects to the super cell. Three different
types of point defects are possible. A vacancy has an attribute called `index`
which defines which atom is going to be deleted from the super cell. A substitutional
atom is created by changing the atom type of the atom with `index` to `atom-type`.
Creating an interstitial atom requires providing the position of the interstitial
(`x`, `y`, `z`) in reduced coordinates of the unit cell and the type of atom that is added.
There is no limit on the number and type of defects added to the super cell, this allows
creating all possible point defect complexes.

Calculating defect formation energies or binding energies between defects can be done
conveniently using :ref:`derived properties <derived_property>`. Point defect
energies are often sensitive to an accurate lattice constant. It is therefore
convenient to set the lattice constant to the relaxed lattice constant of the
corresponding lattice type. This can be done by using the `equalto` attribute
of the `lattice-parameter` property. Usage of a point defect structure is
demonstrated in this :ref:`example <example_derived_properties>`.


.. _relaxation:
.. index:: Relaxation, <relax-dof>, lattice-parameter, atom-coordinates

Relaxation
========================

It is possible to relax the structures during the fitting process. Relaxation of
atom positions is possible for every structure, but the relaxation of other
degrees of freedom like the lattice constant and the ca ratio is currently not
possible for :ref:`user structures <user_defined_structures>`. The relaxation
of all DOFs is controlled by the ``<relax-dof>`` element shown in the following
block::

  <relax-dof>
    <atom-coordinates relax="false"/>
    <lattice-parameter reset="false" />
  </relax-dof>

Mentioning a degree of freedom in this element activates the relaxation of this
DOF. In this example the relaxation of the lattice parameter is activated while
the relaxation of the atom-coordinates is explicitly deactivated by using the
``relax`` flag. The ``reset`` flag controls if the DOF is reset to its initial
value prior to relaxation. This is done by default due to the fact, that bad
parameter sets can lead to a considerable distortion of a structure. If this
distortion is not removed, the relaxation with the next parameter set may take a
long time or even fail completely. If the parameter sets are already decent it
might be better to start every following relaxation from the previous state. If
this is the case ``reset="false"`` may speed up the relaxation process.

Relaxation of a DOF is also automatically activated if it is coupled to a
property that is getting fitted. The following code activates the
``lattice-parameter`` as a fitting property and also as a DOF that is getting
relaxed::
    
    <properties>
      <lattice-parameter fit='true' target='3.615' min='3' max='4' />
    </properties>

Notice the definition of upper and lower bounds for the variation of the lattice
parameter. This is important to make sure, that the distance between atoms does
not exceed the cutoff of the potential.


.. _deformations:
.. index:: Deformations, <deformation>, <deformations>

Deformations
========================

It can be useful to subject a user defined structure to an affine
transformation, e.g., a rescaling of the volume or a deformation of
the cell metric. This can be conveniently accomplished by using the
mechanism provided by the ``<deformation>`` and ``<deformations>``
elements. One first has to define a deformation this is accomplished
by inserting an ``<deformations>`` block *outside* the ``<structures>``
block, which can contain several ``<deformation>`` elements that have to
be named using the ``id`` attribute. Each ``<deformation>`` element
defines a separate deformation to be used later in the definition of
the structures. The ``<deformation>`` element can contain any
combination of the elements ``<scale>`` and ``<matrix>`` as illustrated by
the following code::

  <deformations>
    <deformation id="volume_strain">
      <scale> 1.05 </scale>
    </deformation>

    <deformation id="tetragonal_strain">
      <matrix>
        <scale> 3.615 </scale>
        <s1 x="1.05" y="0.0" z="0.0"/>
        <s2 x="0.0"  y="1.0" z="0.0"/>
        <s3 x="0.0"  y="0.0" z="1.0"/>
      </matrix>
    </deformation>

    ...
  </deformations>

The first deformation rescales the cell metric by a factor of 1.05 and
thus implies a 5% volume strain. The second deformation element
defines a tetragonal strain along the :math:`x` axis via the
``<matrix>`` block; in addition it prescribes a rescaling of the
entire cell metric by 3.615. These deformations can then be used in
the definition of the structures as demonstrated by the following code
snippet::

  <structures>
    <user-structure id="conventional_fcc_cell">
      <pbc x="true" y="true" z="true" />
      <cell>
        <a1 x="1" y="0" z="0" />
        <a2 x="0" y="1" z="0" />
        <a3 x="0" y="0" z="1" />
        <atoms>
          <atom type="Cu" x="0.0" y="0.0" z="0.0" reduced="true" />
          <atom type="Cu" x="0.0" y="0.5" z="0.5" reduced="true" />
          <atom type="Cu" x="0.5" y="0.0" z="0.5" reduced="true" />
          <atom type="Cu" x="0.5" y="0.5" z="0.0" reduced="true" />
        </atoms>
      </cell>
      <deformation>tetragonal_strain</deformation>
    </user-structure>
  </structures>

Here the `tetragonal_strain` deformation is used via the
``<deformation>`` element inside the ``<user-structure>`` construct. The
final cell metric would thus be

.. math::

   \boldsymbol{h} = 3.615 \left(\begin{matrix} 1.05 & 0 & 0 \\
   0 & 1 & 0 \\
   0 & 0 & 1 \end{matrix}\right)

The scaling factor of 3.615 thus here assumes the role of the lattice
constant. The use of deformations is also illustrated by :ref:`a
dedicated example <example_imposing_a_deformation>`.




.. _grouping_structures:

Grouping structures
====================================================

During program execution it is often convenient to assign structures
to different groups, e.g., for splitting the input data into a fit and
a test database. This can be accomplished using the ``<group>``
element. In the following example, the structures in the first group
are not evaluated during the fitting phase (``exclude-fit="true"``)
while the structures in the second group are (``exclude-fit="false"``,
the default behavior); the structures in both groups are included
during the output phase (``exclude-output="false"``)::

  <group exclude-fit="true" exclude-output="false">

    <B1-lattice id="B1_ZnS">
      ...
    </B1-lattice>

    <B2-lattice id="B2_ZnS">
      ...
    </B2-lattice>

  </group>

  <group exclude-fit="false" exclude-output="false">

    <B3-lattice id="B3_ZnS">
      ...
    </B3-lattice>

    <C1-lattice id="C1_ZnS">
      ...
    </C1-lattice>

  </group>
