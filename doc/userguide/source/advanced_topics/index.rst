.. _advanced_topics:

Advanced topics
******************

This section addresses the application of :program:`atomicrex` in more
complex situations. This is done in the form of examples/tutorials.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 1

   mapping_parameter_landscape
   simulated_annealing
