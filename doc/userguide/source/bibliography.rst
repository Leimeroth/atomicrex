.. _bibliography:
.. index:: Bibliography

Bibliography
***************

.. _bibliography_general:


General
======================

.. [Car90]
   A. E. Carlsson,
   *Beyond Pair Potentials in Transition Metals and Semiconductors*,
   Solid State Physics **43**, 1 (1990);
   series published by Academic Press,
   edited by H. Ehrenreich and D. Turnbull
   `doi:10.1088/0965-0393/8/6/305 <http://dx.doi.org/10.1088/0965-0393/8/6/305>`_

.. [StiWeb84]
   F. H. Stillinger and T. A. Weber,
   *Computer simulation of local order in condensed phases of silicon*,
   Phys. Rev. B **31**, 5262 (1984);
   `doi:10.1103/PhysRevB.31.5262 <http://dx.doi.org/10.1103/PhysRevB.31.5262>`_

.. [Fre12]
   D. Frenkel,
   *Simulations: the dark side*,
   arXiv:1211.4440 [cond-mat.stat-mech];
   `download <https://arxiv.org/abs/1211.4440>`_
      
.. _bibliography_tersoff_abop:

Bond-order potentials
======================

.. [Abe85]
   G. C. Abell,
   *Empirical chemical pseudopotential theory of molecular and metallic bonding*,
   Phys. Rev. B **31**, 6148 (1985);
   `doi:10.1103/PhysRevB.31.6184 <http://dx.doi.org/10.1103/PhysRevB.31.6184>`_

.. [Ter86] J. Tersoff,
   *New Empirical Model for the Structural Properties of Silicon*,
   Phys. Rev. Lett. **56**, 632 (1986);
   `doi:10.1103/PhysRevLett.56.632 <http://dx.doi.org/10.1103/PhysRevLett.56.632>`_

.. [Ter88a]
   J. Tersoff,
   *New empirical approach for the structure and energy of covalent systems*,
   Phys. Rev. B **37**, 6991 (1988);
   `doi:10.1103/PhysRevB.37.6991 <http://dx.doi.org/10.1103/PhysRevB.37.6991>`_

.. [Ter88b]
   J. Tersoff,
   *Empirical interatomic potential for silicon with improved elastic properties*,
   Phys. Rev. B **38**, 9902 (1988);
   `doi:10.1103/PhysRevB.38.9902 <http://dx.doi.org/10.1103/PhysRevB.38.9902>`_

.. [Ter88c]
   J. Tersoff,
   *Empirical Interatomic Potential for Carbon, with Applications to Amorphous Carbon*,
   Phys. Rev. Lett. **61**, 2879 (1988);
   `doi:10.1103/PhysRevLett.61.2879 <http://dx.doi.org/10.1103/PhysRevLett.61.2879>`_

.. [Ter89]
   J. Tersoff,
   *Modeling solid-state chemistry: Interatomic potentials for multicomponent systems*,
   Phys. Rev. B. **39**, 5566 (1989);
   `doi:10.1103/PhysRevB.39.5566 <http://dx.doi.org/10.1103/PhysRevB.39.5566>`_

.. [Bre89]
   D. W. Brenner,
   *Relationship between the Embedded-Atom Method and Tersoff Potentials*,
   Phys. Rev. Lett. **63**, 1022 (1989);
   `doi:10.1103/PhysRevLett.63.1022 <http://dx.doi.org/10.1103/PhysRevLett.63.1022>`_

.. [Bre90]
   D. W. Brenner,
   *Empirical potential for hydrocarbons for use in simulating the chemical vapor deposition of diamond films*,
   Phys. Rev. B **42**, 9458 (1990);
   `doi:10.1103/PhysRevB.42.9458 <http://dx.doi.org/10.1103/PhysRevB.42.9458>`_

.. [BreSheHar02]
   D. W. Brenner, O. A. Shenderov, J. A. Harrison, S. J. Stuart, B. Ni and S. B. Sinnott,
   *A second-generation reactive empirical bond order (REBO) potential energy expression for hydrocarbons*,
   J. Phys. Cond. Matter **14**, 783 (2002);
   `doi:10.1088/0953-8984/14/4/312 <http://dx.doi.org/10.1088/0953-8984/14/4/312>`_

.. [AlbNorAve02]
   K. Albe, K. Nordlund, and R. S. Averback,
   *Modeling the metal-semiconductor interaction: Analytical bond-order potential for platinum-carbon*,
   Phys. Rev. B **65**, 195124 (2002);
   `doi:10.1103/PhysRevB.65.195124 <http://dx.doi.org/10.1103/PhysRevB.65.195124>`_

.. [AlbNorNor02]
   K. Albe, K. Nordlund, J. Nord, and A. Kuronen,
   *Modeling of compound semiconductors: Analytical bond-order potential for Ga, As, and GaAs*,
   Phys. Rev. B **66**, 035205 (2002);
   `doi:10.1103/PhysRevB.66.035205 <http://dx.doi.org/10.1103/PhysRevB.66.035205>`_

.. [NorAlbErh03]
   J. Nord, K. Albe, P. Erhart, and K. Nordlund,
   *Modelling of compound semiconductors: Analytical bond-order potential for gallium, nitrogen and gallium nitride},*
   J. Phys. Cond. Matter **15**, 5649 (2003);
   `doi:10.1088/0953-8984/15/32/3245 <http://dx.doi.org/10.1088/0953-8984/15/32/324>`_

.. [ErhAlb05]
   P. Erhart and K. Albe,
   *Analytical Potential for Atomistic Simulations of Silicon and Silicon Carbide*,
   Phys. Rev. B **71**, 035211 (2005);
   `doi:10.1103/PhysRevB.71.035211 <http://dx.doi.org/10.1103/PhysRevB.71.035211>`_

.. [ErhJusGoy06]
   P. Erhart, N. Juslin, O. Goy, K. Nordlund, R. Müller, and K. Albe,
   *Analytic bond-order potential for atomistic simulations of zinc oxide*,
   J. Phys. Cond. Matter **18**, 6585 (2006);
   `doi:10.1088/0953-8984/18/29/003 <http://dx.doi.org/10.1088/0953-8984/18/29/003>`_

.. [JusErhTra05]
   N. Juslin, P. Erhart, P. Träskelin, J. Nord, K. Henriksson, E. Salonen, K. Nordlund, and K. Albe,
   *Analytical interatomic potential for modeling nonequilibrium processes in the W-C-H system*,
   J. Appl. Phys. **98**, 123520 (2005);
   `doi:10.1063/1.2149492 <http://dx.doi.org/10.1063/1.2149492>`_

.. [MulErhAlb07a]
   M. Müller, P. Erhart, and K. Albe,
   *Analytic bond-order potential for bcc and fcc iron - comparison with established embedded-atom method potentials*,
   J. Phys. Cond. Matter **19**, 326220 (2007);
   `doi:10.1088/0953-8984/19/32/326220 <http://dx.doi.org/10.1088/0953-8984/19/32/326220>`_

.. [MulErhAlb07b]
   M. Müller, P. Erhart, and K. Albe,
   *Thermodynamics of L10 ordering in FePt nanoparticles studied by Monte Carlo simulations based on an analytic bond-order potential*,
   Phys. Rev. B **76**, 155412 (2007);
   `doi:10.1103/PhysRevB.76.155412 <http://dx.doi.org/10.1103/PhysRevB.76.155412>`_

.. [PetGreWah15]
   M. V. G. Petisme, M. A. Gren, and G. Wahnström,
   *Molecular dynamics simulation of WC/WC grain boundary sliding resistance in WC–Co cemented carbides at high temperature*,
   Internat. J. Refractory Met. Hard Mater. **49**, 75 (2015);
   `doi:10.1016/j.ijrmhm.2014.07.037 <http://dx.doi.org/10.1016/j.ijrmhm.2014.07.037>`_


.. _bibliography_eam:

Embedding methods
======================

.. [StoZar80]
   M. J. Stott and E. Zaremba,
   *Quasiatoms: An approach to atoms in nonuniform electronic systems*,
   Phys. Rev. B **22**, 1564 (1980);
   `doi:10.1103/PhysRevB.22.1564 <http://dx.doi.org/10.1103/PhysRevB.22.1564>`_

.. [PusNieMan81]
   M. J. Puska, R. M. Nieminen, and M. Manninen,
   *Atoms embedded in an electron gas: Immersion energies*,
   Phys. Rev. B **24**, 3037 (1981);
   `doi:10.1103/PhysRevB.24.3037 <http://dx.doi.org/10.1103/PhysRevB.24.3037>`_

.. [Nor82]
   J. K. Nørskov,
   *Covalent effects in the effective-medium theory of chemical binding: Hydrogen heats of solution in the 3d metals*,
   Phys. Rev. B **26**, 2875 (1982);
   `doi:10.1103/PhysRevB.26.2875 <http://dx.doi.org/10.1103/PhysRevB.26.2875>`_

.. [FinSin84]
   M. W. Finnis and J. E. Sinclair,
   *A simple empirical N-body potential for transition metals*,
   Phil. Mag. A **50**, 45 (1984);
   `doi:10.1080/01418618408244210 <http://dx.doi.org/10.1080/01418618408244210>`_

.. [DawBas83]
   M. S. Daw and M. I. Baskes,
   *Semiempirical, Quantum Mechanical Calculation of Hydrogen Embrittlement in Metals*,
   Phys. Rev. Lett., **50**, 1285 (1983);
   `doi:10.1103/PhysRevLett.50.1285 <http://dx.doi.org/10.1103/PhysRevLett.50.1285>`_

.. [DawBas84]
   M.S. Daw and M. I. Baskes,
   *Embedded-atom method: Derivation and application to impurities, surfaces and other defects in metals*,
   Phys. Rev. B **29**, 6443 (1984);
   `doi:10.1103/PhysRevB.29.6443 <http://dx.doi.org/10.1103/PhysRevB.29.6443>`_

.. [FoiBasDaw86]
   M. S. Daw, S. M. Foiles, and M. I. Baskes,
   *Embedded-atom-method functions for the fcc metals Cu, Ag, Au, Ni, Pd, Pt, and their alloys*,
   Phys. Rev. B **33**, 7983 (1986);
   `doi:10.1103/PhysRevB.33.7983 <http://dx.doi.org/10.1103/PhysRevB.33.7983>`_

.. [DawFoiBas93]
   M. S. Daw, S. M. Foiles, and M. I. Baskes,
   *The embedded-atom method - A review of theory and applications*,
   Mater. Sci. Rep. **9**, 251 (1993);
   `doi:10.1016/0920-2307(93)90001-U <http://dx.doi.org/10.1016/0920-2307(93)90001-U>`_

.. [ErcTosPar86]
   F. Ercolessi, E. Tosatti, and M. Parrinello,
   *Au (100) Surface Reconstruction*,
   Phys. Rev. Lett. **57**, 719 (1986);
   `doi:10.1103/PhysRevLett.57.719 <http://dx.doi.org/10.1103/PhysRevLett.57.719>`_

.. [ErcAda94]
   F. Ercolessi and J. B. Adams,
   *Interatomic potentials from first-principles calculations: the force-matching method*,
   Europhys. Lett. **26**, 583 (1994);
   `doi:10.1209/0295-5075/26/8/005 <http://dx.doi.org/10.1209/0295-5075/26/8/005>`_

.. [MisMehPap01]
   Y. Mishin, M. J. Mehl, D. A. Papaconstantopoulos, A. F. Voter, and  J. D. Kress,
   *Structural stability and lattice defects in copper: Ab initio, tight-binding, and embedded-atom calculations*,
   Phys. Rev. B **63**, 224106 (2001);
   `doi:10.1103/PhysRevB.63.224106 <http://dx.doi.org/10.1103/PhysRevB.63.224106>`_


.. _bibliography_meam:

Modified embedded atom method
================================

.. [Bas87]
   M. Baskes,
   *Application of the Embedded-Atom Method to Covalent Materials: A Semiempirical Potential for Silicon*,
   Phys. Rev. Lett. **59**, 2666 (1987);
   `doi:10.1103/PhysRevLett.59.2666 <http://dx.doi.org/10.1103/PhysRevLett.59.2666>`_

.. [Bas92]
   M. Baskes,
   *Modified embedded-atom potentials for cubic materials and impurities*,
   Phys. Rev. B **46**, 2727 (1992);
   `doi:10.1103/PhysRevB.46.2727 <http://dx.doi.org/10.1103/PhysRevB.46.2727>`_

.. [LeeBasKim01]
   B.-J. Lee, M. I. Baskes, H. Kim, and Y. K. Cho,
   *Second nearest-neighbor modified embedded atom method potentials for bcc transition metals*,
   Phys. Rev. B **64**, 184102 (2001);
   `doi:10.1103/PhysRevB.64.184102 <http://dx.doi.org/10.1103/PhysRevB.64.184102>`_

.. [LenSadAlo00]
   T. J. Lenosky, B. Sadigh, Babak, E. Alonso, V. V. Bulatov, T. Diaz de la Rubia, J. Kim, A. F. Voter, and J. D. Kress,
   *Highly optimized empirical potential model of silicon*,
   Modelling Simul. Mater. Sci. Eng. **8**, 825 (2000);
   `doi:10.1088/0965-0393/8/6/305 <http://dx.doi.org/10.1088/0965-0393/8/6/305>`_


.. _bibliography_adp:

Angular dependent potential
================================

.. [MisMehPap05]
   Y. Mishin, M.J. Mehl, D.A. Papaconstantopoulos,
   *Phase stability in the Fe–Ni system: Investigation by first-principles calculations and atomistic simulations*,
   Acta Mater. **53**, 4029 (2005);
   `doi:10.1016/j.actamat.2005.05.001 <https://doi.org/10.1016/j.actamat.2005.05.001>`_

