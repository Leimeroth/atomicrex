.. _Angular_Dependent_potential:
.. index::
   single: Angular Dependent Potential (ADP); description
   single: Angular Dependent Potential (ADP); parameters
   single: <adp>

Angular dependent potential
=======================================

The angular dependent potential (ADP) was proposed by Mishin et al. [MisMehPap05]_ and extends the EAM formalism
with angular dependent dipole and quadrupole terms.

.. math::
  E = \frac{1}{2} \sum_{ij} V(r_{ij}) + \sum_i F(\rho_i) + \frac{1}{2} \sum_{i,\alpha} (\mu_i^{\alpha})^2 + \frac{1}{2} \sum_{i,\alpha,\beta} (\lambda_i^{\alpha,\beta})^2 - \frac{1}{6} \sum_i \eta_i^2

where

.. math::
   \rho_i = \sum_j \rho_j(r_{ij}),

.. math::
  \mu_i^{\alpha} = \sum_{j\neq i} u_{ij}(r_{ij})r_{ij}^{\alpha},


.. math::
  \lambda_i^{\alpha,\beta} = \sum_{j\neq i} w_{ij}(r_{ij})r_{ij}^{\alpha}r_{ij}^{\beta}

and

.. math::
  \eta_i = \sum_{\alpha} \lambda_i^{\alpha\alpha}


The functions :math:`V`, :math:`F`, :math:`\rho`, :math:`u` and :math:`w` can be chosen freely
using a math parser :ref:`compose functional
forms <user_defined_functions>`, similar to EAM potentials.
The current implementation allows the electron density of EAM potentials to be defined
for each pair of elements separately (i.e. for 2 Elements 3 functions can be defined).
For ADPs the electron density at the position of some atom
is only defined by the distance and species of the atoms surrounding it.
Consequently, :math:`\rho` requires to set only one species.
Furthermore, the additional function :math:`u` and :math:`w`
have to be defined.
The following code block illustrates the differences compared to setting up an EAM potential of a rather simple
functional form. The main block is called ``<adp>``.

.. code:: xml

   <adp id="ADP_SAMPLE" species-a="*" species-b="*">
     <export-adp-file resolution='10000' rho-range-factor='2.0'>output.adp</export-adp-file>

     <mapping>
       <pair-interaction species-a="*" species-b="*" function="V" />
       <electron-density species="*" function="rho" />
       <embedding-energy species="*" function="F" />
       <u-function species-a="*" species-b="*" function="u" />
       <w-function species-a="*" species-b="*" function="w" />
     </mapping>

     <functions>
       <user-function id="V">
         Same as EAM
       </user-function>

       <user-function id="rho">
         Same as EAM, but only 1 per species
       </user-function>

       <user-function id="F">
         Same as EAM
       </user-function>

       <user-function id="i">
         <input-var>r</input-var>
         <expression> exp(-twomu*r) </expression>
         <derivative> -twomu*exp(-twomu*r) </derivative>
         <param name="twomu">1.14</param>
         <fit-dof> <twomu/> </fit-dof>
         <screening>
           <user-function id="rho_screening">
             <cutoff>6.5</cutoff>
             <input-var>r</input-var>
             <expression> 1 - 1/(1 + ((r - cutoff) / h)^4) </expression>
             <derivative> 4 * h^4 * (r - cutoff)^3 / ((h^4 + (r - cutoff)^4)^2) </derivative>
             <param name="h">3</param>
           </user-function>
         </screening>
       </user-function>

       <user-function id="w">
         <input-var>r</input-var>
         <expression> exp(-twomu*r) </expression>
         <derivative> -twomu*exp(-twomu*r) </derivative>
         <param name="twomu">1.14</param>
         <fit-dof> <twomu/> </fit-dof>
         <screening>
           <user-function id="rho_screening">
             <cutoff>6.5</cutoff>
             <input-var>r</input-var>
             <expression> 1 - 1/(1 + ((r - cutoff) / h)^4) </expression>
             <derivative> 4 * h^4 * (r - cutoff)^3 / ((h^4 + (r - cutoff)^4)^2) </derivative>
             <param name="h">3</param>
           </user-function>
         </screening>
       </user-function>

     </functions>
   </adp>


.. rubric:: Further information

For further information on ADPs refer to the original publication [MisMehPap05]_.
