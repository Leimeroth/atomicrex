.. _potentials:

.. index::
   single: Potentials
   single: Potentials; overview
   single: Potentials; common parameters
   single: <potentials>

Potentials
**********

:program:`atomicrex` supports a number of different potential types
that are :ref:`listed at the end of this section <list_of_potentials>`
and described in the following sections.
The potential type(s) are specified in the ``<potentials>`` block of
the main input file. It can be useful to move this block to a separate
file and incorporate it via :ref:`XML inclusions <xml_inclusions>` as
demonstrated in :ref:`this example
<example_potential_EAM>`. The ``<potentials>`` can
comprise several subblocks, which define several different
potentials. It is thereby possible to combine (superimpose) several
potential types. The potential specific blocks share several
attributes: the ``id`` attribute is used to set a unique identifier
for each potential; ``species-a`` and ``species-b`` identify which
:ref:`atom types <atom_types>` the potential is dealing with. The use
of the attributes is illustrated in the following code segment::

  <potentials>
    <eam id="Al" species-a="Al" species-b="Al">
    ...
    </eam>
    <eam id="SiAl" species-a="Si" species-b="Al">
    ...
    </eam>
    <abop id="Si" species-a="Si" species-b="Si">
    ...
    </eam>
  </potentials>

.. _fitdof:

.. index::
   single: <fit-dof>

Each potential is associated with a set of parameters that represent
the :ref:`degrees of freedom <dof>` of the potential. The user has to
specify which parameters are allowed to vary during the optimization
process (and are thus "dynamic" DOFs as opposed to "static"
DOFs). This is accomplished by including a ``<fit-dof>`` block
*inside* the respective potential block that contains a list of
elements named by the parameter they are associated with. These
elements have several attributes:

* The ``enabled`` attribute allows one to switch the DOF from dynamic
  (``False``) to static (``True``). This allows one to include or exclude
  a certain parameter from the fit.
* The ``reset`` attribute specifies whether the DOF is reset to the
  initial value specified by the user each time a new relaxation round
  is performed.
* The ``min`` and ``max`` attributes specify the range, within which the
  parameter is allowed to vary.
* The ``tag`` attribute is used by some potential forms such as
  :ref:`Tersoff <tersoff_potential>` and :ref:`analytic bond-order
  <abop_potential>` potentials to associate parameters with certain
  mixed interactions.

A ``<fit-dof>`` block could thus look as follows::

  <fit-dof>
    <twomu enabled="True" min="0.1" max="2.0">
    <beta enabled="False">
  </fit-dof>

More examples can be found in the following sections and in the
:ref:`examples section <examples>`.

.. _list_of_potentials:

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 2

   lj
   eam
   tersoff
   abop
   meam
   angular_dependent_potential
   tabulated
   functions
