.. _meam_potential:
.. index::
   single: Modified embedded atom method (MEAM); description
   single: Modified embedded atom method (MEAM); parameters
   single: Cluster functionals; Modified embedded atom method (MEAM)
   single: Three-body potentials; Modified embedded atom method (MEAM)
   single: <meam>

Modified embedded atom method
==========================================

The modified embedded atom method (MEAM) potential scheme was
developed as a generalization of the :ref:`embedded atom method
<eam_potential>` [Bas87]_. In contrast to the latter, MEAM potentials
include angular dependent interactions, which enter via the electron
density term. As a result these potentials can describe directional
bonding, which is most apparent in covalent materials such as silicon
and diamond but also in e.g., body-centered cubic (BCC) metals
[Bas92]_.

While initially simple analytic forms were adopted and the (angular)
interactions were restricted to first-nearest neigbors [Bas87]_,
[Bas92]_, the MEAM form can be easily extended to include a larger
number of neighbors [LeeBasKim01]_ and support general functional
forms (e.g., splines [LenSadAlo00]_). According to the classification
of Carlsson [Car90]_ MEAM potentials fall in the category of cluster
functionals, which also includes e.g., :ref:`analytic bond order
potential <abop_potential>`.

The general form for the total energy is (also compare the
:ref:`embedded atom method (EAM) format <eam_potential>`)

.. math::
   E = \sum_{ij} V(r_{ij}) + \sum_i F(\rho_i)

where the angular dependence enters via the electron density term

.. math::
   \rho_i = \sum_j \rho(r_{ij})
   + \sum_{jk} f(r_{ij}) f(r_{ik}) g\left(\cos(\theta_{ijk})\right).

The second term in the summation constitutes they key difference
compared to the :ref:`EAM format <eam_potential>`.

As in the case of EAM potentials various functional forms have been
proposed for :math:`V`, :math:`F`, :math:`\rho`, :math:`f`, and
:math:`g`. In the present implementation it is possible to
:ref:`compose functional forms <user_defined_functions>` using a math
parser. This allows one not only to replicate any of the original
forms but to define practically arbitrary functional forms. This is
demonstrated by the :ref:`construction of a MEAM potential with user
defined functions <example_potential_meam>`.

The following code block illustrates the definition of a rather simple
functional form.  The two main subelements of the ``<meam>`` block are
``<mapping>`` and ``<functions>``.  The ``<functions>`` block
comprises the definitions of the various functions and parameters as
described in detail in the section on the :ref:`specification of
functional forms <function_definition>`. Each function defined has to
be assigned an id using the ``id`` attribute. The id is used in the
``<mapping>`` block to attach the functions to certain types of
interaction (pair potential ``V``, electron density ``rho`` along with
the supporting functions ``f`` and ``g``) or atom type (embedding
function ``F``).

.. code:: xml

   <meam id="Al" species-a="*" species-b="*">
     <export-functions>Si_meam_out</export-functions>

     <mapping>
       <pair-interaction species-a='*' species-b='*' function='V' />
       <electron-density species-a='*' species-b='*' function='rho' />
       <f-function species-a='*' species-b='*' function='f' />
       <g-function species-a='*' species-b='*' species-c='*' function='g' />
       <embedding-energy species='*' function='F' />
     </mapping>

     <functions>
       <spline id='V'>
         <derivative-left>-42.66967</derivative-left>
         <cutoff>4.5</cutoff>
         <nodes>
         <node x='1.500000000'  y=' 6.92994'   enabled='true' />
         <node x='1.833333333'  y='-0.43995'   enabled='true' />
         <node x='2.166666667'  y='-1.70123'   enabled='true' />
         <node x='2.500000000'  y='-1.62473'   enabled='true' />
         <node x='2.833333333'  y='-0.99696'   enabled='true' />
         <node x='3.166666667'  y='-0.27391'   enabled='true' />
         <node x='3.500000000'  y='-0.02499'   enabled='true' />
         <node x='3.833333333'  y='-0.01784'   enabled='true' />
         <node x='4.166666667'  y='-0.00961'   enabled='true' />
         <node x='4.500000000'  y=' 0.0    '   enabled='true' />
         </nodes>
       </spline>
       ...
     </functions>
   </meam>



.. rubric:: Elements and attributes

.. index:: <mapping>, <pair-interaction>, <electron-density>,
           <embedding-energy>, <f-function>, <g-function>

* ``<mapping>``: This block defines the mapping of the functions defined
  in the ``<functions>`` block onto different atom types and pairs of
  atom types.

  * ``<pair-interaction>``: Assign a pair interaction using the
    attributes ``species-a`` and ``species-b`` to specify the atom types
    involved and the ``function`` attribute to specify the id of the
    function. The function id has to match exactly one of the
    functions defined in the ``<functions>`` block.
  * ``<electron-density>``: Assign an electron density function using
    the attributes ``species-a`` and ``species-b`` to specify the atom
    types involved and the ``function`` attribute to specify the id of
    the function. The function id has to match exactly one of the
    functions defined in the ``<functions>`` block.
  * ``<f-function>``: Assign a supporting pairwise function that
    enters the angular dependent part of the electron density
    calculation using the attributes ``species-a`` and ``species-b`` to
    specify the atom types involved and the ``function`` attribute to
    specify the id of the function. The function id has to match
    exactly one of the functions defined in the ``<functions>`` block.
  * ``<g-function>``: Assign a supporting angular function that enters
    the angular dependent part of the electron density calculation
    using the attributes ``species-a``, ``species-b``, and ``species-c`` to
    specify the atom types involved and the ``function`` attribute to
    specify the id of the function. The function id has to match
    exactly one of the functions defined in the ``<functions>`` block.
  * ``<embedding-energy>``: Assign an embedding function using the
    attribute ``species`` to specify the atom type involved and the
    ``function`` attribute to specify the id of the function. The
    function id has to match exactly one of the functions defined in
    the ``<functions>`` block.

.. index:: <functions>, <export-functions>

* ``<functions>``: This block comprises the definitions of the various
  functions and parameters. The definition of a function is described
  :ref:``here <function_definition>``. Each function defined has to be
  assigned an id using the ``id`` attribute. The id is used in the
  ``<mapping>`` block to attach a function to a certain type of
  interaction (pair potential and electron density) or atom type
  (embedding function).

* ``<export-functions>`` (optional): Name of file, to which potential
  parameters are being written in Lammps format [Default: no file is
  written].


.. rubric:: Further information

More detailed information can be found in [Bas87]_, [Bas92]_, [LeeBasKim01]_,
[LenSadAlo00]_.
