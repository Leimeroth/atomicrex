from atomicrex.ase_calculator import Atomicrex
from ase.lattice.cubic import BodyCenteredCubic
import ase
import numpy as np

__doc__ = """This script compares the results of the calculator to
LAMMPS results.  """

# create super cell and vacancy
atoms = BodyCenteredCubic(size=(2, 2, 2), symbol='Fe', latticeconstant=2.86)
del atoms[0]

# create calculator
calc = Atomicrex(file_name="tersoff.xml")
atoms.set_calculator(calc)

# check forces
f = atoms.get_forces()
assert abs(f[0][0] - -0.198631) < 1e-5
assert abs(f[1][0] - 0.0) < 1e-9

# check against numerical forces
try:
    # calculate_numerical_forces has only been added in 3.10.x (probably)
    diff = calc.calculate_numerical_forces(atoms) - f
    assert np.all(diff < 1e-6)
except AttributeError:
    pass

# check energy
e = atoms.get_potential_energy()
assert abs(e - -62.405429) < 1e-3

# check stress
p = atoms.get_stress()
assert abs(p[0] - 1.55450840e-02) < 1e-7
assert abs(p[1] - 1.55450840e-02) < 1e-7
assert abs(p[2] - 1.55450840e-02) < 1e-7
assert abs(p[3] - 0.0) < 1e-9
assert abs(p[4] - 0.0) < 1e-9
assert abs(p[5] - 0.0) < 1e-9
try:
    # calculate_numerical_forces has only been added in 3.10.x (probably)
    diff = calc.calculate_numerical_stress(atoms) - p
    assert np.all(diff < 1e-6)
except AttributeError:
    pass
