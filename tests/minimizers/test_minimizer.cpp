#define BOOST_TEST_MODULE minimizer
#include <boost/test/included/unit_test.hpp>
#include <arexlib/Atomicrex.h>
#include <arexlib/job/FitJob.h>

using namespace atomicrex;

BOOST_AUTO_TEST_CASE(internal_bfgs)
{
    FitJob job;
    job.parse("main-internal-bfgs.xml");
    job.performFitting();
    BOOST_CHECK_CLOSE(job.calculateResidual(), 0.219295, 0.001);
}

BOOST_AUTO_TEST_CASE(internal_spa)
{
    FitJob job;
    job.parse("main-internal-spa.xml");
    job.performFitting();
    BOOST_CHECK_CLOSE(job.calculateResidual(), 47187.4, 0.001);
}

BOOST_AUTO_TEST_CASE(nlopt_bfgs)
{
    FitJob job;
    job.parse("main-nlopt-bfgs.xml");
    job.performFitting();
    BOOST_CHECK_CLOSE(job.calculateResidual(), 1192.06, 0.001);
}

BOOST_AUTO_TEST_CASE(nlopt_sbplx)
{
    FitJob job;
    job.parse("main-nlopt-sbplx.xml");
    job.performFitting();
    BOOST_CHECK_CLOSE(job.calculateResidual(), 167738, 0.001);
}
