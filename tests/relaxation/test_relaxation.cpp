#define BOOST_TEST_MODULE relaxation
#include <boost/test/included/unit_test.hpp>
#include <arexlib/Atomicrex.h>
#include <arexlib/job/FitJob.h>

using namespace atomicrex;

BOOST_AUTO_TEST_CASE(user_structure)
{
    FitJob job;
    job.parse("main.xml");
    for(AtomicStructure* structure : job.structures()) {
        // Compute property values.
        structure->computeProperties(false);

        if(structure->id() == "default" || structure->id() == "full") {
            // BOOST_CHECK_CLOSE(static_cast<ScalarFitProperty*>(structure->propertyById("atomic-energy"))->computedValue(),
            // -4.28, 0.001);
            BOOST_CHECK_CLOSE(structure->totalEnergy(), -8.56, 0.001);
            BOOST_CHECK_CLOSE(structure->pressure(), -2.45787, 0.001);
            BOOST_CHECK_CLOSE(structure->volume(), 23.3985, 0.001);
        }
        else if(structure->id() == "hydrostatic") {
            BOOST_CHECK_CLOSE(structure->totalEnergy(), -8.46391, 0.001);
            BOOST_CHECK_CLOSE(structure->pressure(), -1180.385, 0.001);
            BOOST_CHECK_CLOSE(structure->volume(), 23.3585, 0.001);
        }
        else if(structure->id() == "constant-volume") {
            BOOST_CHECK_CLOSE(structure->totalEnergy(), -8.30922, 0.001);
            BOOST_CHECK_CLOSE(structure->pressure(), 309536.5, 0.001);
            BOOST_CHECK_CLOSE(structure->volume(), 20.4592, 0.001);
        }
        else if(structure->id() == "none") {
            BOOST_CHECK_CLOSE(structure->totalEnergy(), -8.15856, 0.001);
            BOOST_CHECK_CLOSE(structure->pressure(), 340679.6, 0.001);
            BOOST_CHECK_CLOSE(structure->volume(), 20.3479, 0.001);
        }
    }
}
